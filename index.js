const Discord         = require('discord.js');
const celest          = new Discord.Client();
const brain           = require('./Brain/random');
const event           = require('./Brain/event');
const sqlFunctions    = require('./Brain/sqlFunctions');
const v146            = require('./Brain/v146Con');
const help            = require('./Brain/help');
const marvel          = require(`./marvel/marvel`);
const gambel          = require(`./gambel/gambel`);
const gacha           = require(`./gambel/gacha`);
const react           = require(`./reactions/react`);
const play            = require(`./gambel/2playerflip`);
const door            = require(`./gambel/door`);
const gainItem        = require(`./gambel/randomGains`);
const jackpot         = require(`./gambel/jackpot`);
const account         = require(`./account/register`);
const vote            = require(`./account/vote`);
const update          = require(`./account/207`);
const tester          = require(`./account/testaccount`);
const og              = require(`./account/og`);
const ign             = require(`./account/ignRes`);
const igntest         = require(`./account/ignRestest`);
const msgCheck        = require(`./account/rewards`);
const link            = require(`./account/link`);
const ranking         = require(`./account/ranking`);
const character       = require(`./account/character`);
const trade           = require(`./account/give`);
const accountInfo     = require(`./account/account`);
const fame            = require(`./account/fame`);
const donate          = require(`./account/donate`);
const marry           = require(`./account/disMarriage`); 
const unstuck         = require(`./account/unstuck`);
const newMember       = require(`./Brain/newMember`);
const music           = require(`./Brain/music`);
const celestOn        = require(`./Brain/Celest`);
const settings        = require(`./Brain/settings`);
const sha1            = require('sha1');
celest.music          = require("discord.js-musicbot-addon");


//Calls newMember, And sends channel and player a message.
newMember.joinMessage(celest);

//Displays the start information in the console.
celestOn.celestStart(celest);


celest.music.start(celest, {
  youtubeKey: settings.youtubeAPI3,
  anyoneCanSkip: true,
  logging: false,
  botPrefix: `lsakfh;FHSjkhfd`,
  cooldown: {
    enabled: false
  }
});

let prefix = 'celest ';

celest.on('guildMemberRemove',(member) => {
  celest.channels.get('610570550472867853').send(`>>> __**${guild.member.displayName}**__ Just left Celestial`);
  return;
});

celest.on('messageDelete', message => {
  //console.log(`a message saying "${message.cleanContent}" was deleted from channel: ${message.channel.name} at ${new Date()}`);
  let deleteEmbed = new Discord.RichEmbed({
    title: `Deleted Message Log`,
    fields: [
      {
        name: `Deleted Message`,
        value: `${message.cleanContent}`
      },
      {
        name: `Message posted by`,
        value: `${message.author}`
      }
    ],
    footer: {
      text: `${new Date()}`
    }
  });
  celest.channels.get("610570550472867853").send(deleteEmbed)
});

celest.on('message', msg => {

  
  if (msg.content.includes('celestial')) return;
  if (msg.author.bot) return;
  let discordID = msg.author.id;

  
let bannedWords = [`nigger`, `faggot`, `n1gger`, `nlgger`, `n i g g e r`, `𝗇𝗂𝗀𝗀𝖾𝗋`];

//let currentCommand = new Array;

    for(var i = 0; i < bannedWords.length; i++){
      if (msg.content.toLowerCase().includes(bannedWords[i])) {
        sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
          if (results[0]) {
            let rows = JSON.parse(JSON.stringify(results[0]));
            let warn = rows.warn;
            switch (warn) {
              case 0:
                warn = 1;
              break;
              case 1:
                warn = 2;
              break;
              case 2:
                warn = 3;
              break;
            }
            if (warn == 3) {
              msg.channel.send(`__**${msg.member.displayName}**__ has been Permanantly banned from Celestial after 3 Warnings.`);
              msg.member.ban();
              return;
            }
            msg.reply(`this is your ${warn} warning. You will be Permanantly banned at 3.`);
            sqlFunctions.getResult(`UPDATE accounts SET warn=${warn} WHERE discordID=${discordID}`, function() {})
          } else {
            msg.member.ban();
            msg.channel.send(`__**${msg.member.displayName}**__ has been Permanantly banned from Celestial.`);
          }
        });
        return;
      }
    }


  if (msg.content.toLowerCase().includes(`@everyone`)) {
    return;
  }

  if (msg.content.toLowerCase().includes(`@here`)) {
    return;
  }

  //return;
    
    

    if (msg.content.toLowerCase().includes(`players online`)) {
      sqlFunctions.getResult(`SELECT count(*) AS online FROM accounts WHERE loggedin=2`, function(err, results) {
        let online = JSON.parse(JSON.stringify(results[0]));
        console.log(online.online);
        msg.channel.send(`There are currently ${online.online} players on CelestialMS right now!`);
      });
    }


    const args = msg.content.trim().split(/ +/g);

    let randomChannel = brain.msgChannel[Math.floor(Math.random() * brain.msgChannel.length)];
    let randomThumb   = brain.embedImg[Math.floor(Math.random() * brain.embedImg.length)];
    let noRandom = brain.noCommand[Math.floor(Math.random() * brain.noCommand.length)];
    let animes = brain.animeRecco[Math.floor(Math.random() * brain.animeRecco.length)];
    let luffReply = brain.loveReply[Math.floor(Math.random() * brain.loveReply.length)];
    let hateReply = brain.hate[Math.floor(Math.random() * brain.hate.length)];

    let msgText = msg.content;


    

    if (msg.content.toLowerCase().includes('info')) {
      help.doInfo(msg, discordID, randomThumb, args);
    }

    if (msg.content.toLowerCase() === `help`) {
      //console.log(msg.content);
      help.doHelp(msg, discordID, randomThumb, args);
      return;
    }

    if (msg.channel.type !== 'dm' && msg.channel.id === '601996201386180619') {
        msgCheck.doRewards(msg, discordID, randomThumb, args);
      }

      if (msg.channel.type === 'dm') {
          
        }
        donate.donate(msg, discordID, randomThumb, args);
        update.account(msg, discordID, randomThumb, args);
        tester.account(msg, discordID, randomThumb, args);
        //og.account(msg, discordID, randomThumb, args);
        ign.ign(msg, discordID, randomThumb, args);
        igntest.ign(msg, discordID, randomThumb, args);
        link.account(msg, discordID, randomThumb, args);

        if (msg.content.toLowerCase().includes('stuck')) {
          unstuck.unstuck(msg, discordID);
          return;
        }

        if (msg.content.toLowerCase().startsWith(`download`)) {
          let download = new Discord.RichEmbed({
            title: `<a:google:611483067353268225>Download CelestialMS<a:google:611483067353268225>`,
            fields: [
              {
                name:`You need to have v208.3 Downloaded, If you don't you can download it below`,
                value: `
                [v203.8 Direct](http://celestialms.com/208.rar)\r
                [v203.8 Google Drive](https://drive.google.com/open?id=1j3niwm9Gb6s_VEhHQZUPFAhiKRZPqR2p)`,
                inline: false,
              },
              {
                name:`Once you have v208.3 downloaded download Our client below!`,
                value: `
                [Direct Link](http://celestialms.com/Celest.rar)\r
                [Google Drive Link](https://drive.google.com/open?id=1Dpttuep1Fv_BAT0FE8JSK_fLfguXNfEb)\r
                [Mediafire Link](http://www.mediafire.com/file/kpoaf87ls4gv4dr/Celest.rar)\r\r`,
                inline: false,
              },
              {
                name:`__**FAQ**__`,
                value: `
                ***I'm connecting to GMS, what do I do?*** \rMake sure You dragged all 3 files from the rar folder into your v208.3 folder\r\r
                ***Client gets deleted when I extract it*** \rEither Turn off your antivirus, or allow celestiallauncher.exe as an exception\r\r
                ***Nexon Patcher opens*** \rYou aren't running the correct version or you didn't replace the files correctly`,
                inline: false,
              }
            ],
          });
          msg.channel.send(download);
        }

        if (msg.channel.id !== `610984831337365531`) { 
          if (msg.channel.id !== `601996201386180619`) {
            if (msg.channel.id !== `601998408093401108`) {
              if (msg.channel.id !== `604752530760663051`) {
                if (msg.channel.id !== `608484416401965056`) {
                  if (msg.channel.id !== `614309332129021960`) {
                    if (msg.channel.type !== `dm`) {
                      return;
                    }
                  }
                }
              }
            }
          }
        }



        character.showCharacter(msg, discordID, randomThumb, args);


        react.reactions(msg, discordID, randomThumb, args);
      
    
      //play.doGambel(msg, discordID, randomThumb, args);

      accountInfo.account(msg, discordID, randomThumb, args, celest);

      gainItem.Random(msg, discordID, randomThumb, args, celest);

      marry.doMariage(msg, discordID, randomThumb, args);
      
      event.doEvent(msg, discordID, randomThumb, args);


      if (msg.content.toLowerCase() === `register`) {
        if (msg.channel.type === 'dm' && msg.content.toLowerCase().includes('register')) {
          account.doRegister(msg, discordID, randomThumb, args, celest);
          return;
      } else {
        msg.channel.send(`the register command needs to be used in my DMs!`);
      }
      }

    if (!msg.content.toLowerCase().includes(prefix)) return;

    if (msg.content.includes(".") && msg.channel.id !== `610984831337365531`) {
      msg.channel.send("Any further attempts at Exploits will result in a Permanant suspension of your discord and in game account.");
      return;
    }

  
    

    music.getMusic(msg, discordID, randomThumb, args, celest, prefix);





let celestCommands = [`anime`, `love`, `hate`, `marvel`,
                      `gacha`, `flip`, `give`, `ranking`, `report`,
                      `vote`, `door`, `unstuck`, `marry`, `help`, `apply`, `fame`, `cuddle`, `jackpot`];

//let currentCommand = new Array;

for(var i = 0; i < celestCommands.length; i++){

if (msg.content.toLowerCase().includes(celestCommands[i])) {

  let whatever = celestCommands[i];

    switch (whatever) {
      case `anime`:
        msg.channel.send(animes);
      break;

      case `love`:
        msg.channel.send(luffReply);
      break;

      case `cuddle`:
        msg.channel.send(cuddleReply);
      break;

      case `hate`:
        msg.channel.send(hateReply);
      break;

      case `marvel`:
        marvel.doMarvel(msg, discordID);
      break;

      case `gacha`:
        gacha.doGacha(msg, discordID, args, celest);
      break;

      case `flip`:
        gambel.doGambel(msg, discordID, randomThumb, args, prefix, celest);
      break;

      case `jackpot`:
        jackpot.jackpot(msg, discordID, randomThumb, args, prefix, celest);
      break;

      case `give`:
        trade.doGive(msg, discordID, randomThumb, args);
      break;

      case `ranking`:
        ranking.doRanking(msg, discordID, randomThumb, args);
      break;

      case `vote`:
        vote.doVote(msg, discordID, randomThumb, args);
      break;

      case `help`:
        help.doHelp(msg, discordID, randomThumb, args);
      break;
      case `apply`:
        event.application(msg, discordID, randomThumb, args, celest);
      break;
      case `report`:
        event.report208(msg, discordID, randomThumb, args, celest);
      break;
      case `fame`:
        fame.fame(msg, discordID, randomThumb, args, celest);
      break;

      /*case `door`:
        door.startDoor(msg, discordID, randomThumb);
      break;*/

      default:
        msg.channel.send(noRandom);
      break;
    }
    break;
  }
}// XXX:
});



celest.login(settings.loginToken);


// end of file
