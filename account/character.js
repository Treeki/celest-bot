const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');
var jobs = require('../Brain/jobs');



module.exports.showCharacter = function(msg, discordID, randomThumb, args) {

    if (msg.content.toLowerCase().includes(`character`)) {

        if (args[0] !== `character`) {
            return;
        }
        console.log(`🛡Displaying character info for ${msg.author.username}..🛡`);




        sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
            if (results[0]) {


                let accRows = JSON.parse(JSON.stringify(results[0]));

                let accountID = accRows.id;

               // console.log(accountID);

            sqlFunctions.getResult(`SELECT * FROM characters WHERE accountid = ${accountID}`, function(err, results) {

                if (results[0]) {
            
                    let charRows = JSON.parse(JSON.stringify(results[0]));

                    //Lets grab all info about the first character that's grabbed
                    let map = charRows.map;
                    let name = charRows.name;
                    let fame = charRows.fame;
                    let gm = charRows.gm;
                    let guild = charRows.guildid;
                    let job = charRows.job;
                    let level = charRows.level;
                    let id = charRows.id;

                    //let guild = charRows.guildid;
                    let guildrank = charRows.guildrank;

                    let eyes = charRows.face;
                    let hair = charRows.hair;

                    let mesos = charRows.meso;

                    let charFeilds = new Array;
                    let charItems = new Array;

                    let addEquip = new Array;

                    let weapon = new Array;
                    let top = new Array;
                    let bottom = new Array;
                    let overall = new Array;
                    let shoes = new Array;
                    let earrings = new Array;
                    let cape = new Array;
                    let faceacc = new Array;
                    let hat = new Array;

                    sqlFunctions.getResult(`SELECT * FROM inventoryitems WHERE characterid = ${id} && inventorytype = -1`, function(err, results) {
                        if (results[0]) {
                            for(var i = 0; i < results.length; i++){
                                var rows = JSON.parse(JSON.stringify(results[i]));
                                charItems.push(rows.itemid);
                                let currentEquip = `${rows.itemid}`;
                                let thisEquip = currentEquip.substring(0, 3);

                                
                                if (thisEquip) {

                                //console.log(thisEquip);
                                switch (thisEquip) {
                                    case `100`: // Hat
                                        hat.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `105`: //Overall
                                        overall.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `103`: //Earring
                                        earrings.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `104`: //Top
                                        top.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `110`: // Cape
                                        cape.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `101`: //FaceAcc
                                        faceacc.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `106`: //Pants
                                        bottom.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `107`: //shoes
                                        shoes.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `170`: //NX Weapon
                                        weapon.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                    case `130`: //1H Sword
                                        weapon.push(rows.itemid);
                                        addEquip.push(`{"itemId":${rows.itemid}}`);
                                    break;
                                  }
                                }

                    /*console.log(`Currently Equipped Items:
                    \rHat: ${hat}
                    \rOverall: ${overall}
                    \rEarring: ${earrings}
                    \rTop: ${top}
                    \rPants: ${bottom}
                    \rShoes: ${shoes}
                    \rWeapon: ${weapon}
                    \rCape: ${cape}`);*/

                            }
                        
                    }

                    sqlFunctions.getResult(`SELECT * FROM guilds WHERE guildid = ${guild}`, function(err, results) {
                        
                        

                        let guildText = `${name} is not in a guild!`;
                    if (results[0]) {
                        var guildrows = JSON.parse(JSON.stringify(results[0]));
                        guildText = `${guildrows.name}`;
                    }
                
                    
                        
                   


                   // console.log(addEquip.toString());
                

                    

                    

                    const firstChar = new Discord.RichEmbed({
                        title: `Your Character ${name}`,
                        fields: charFeilds,
                    });

                    let gmFeild = Object.assign({}, firstChar.fields[0]);
                    let fameFeild = Object.assign({}, firstChar.fields[1]);
                    let guildFeild = Object.assign({}, firstChar.fields[2]);
                    let JobFeild = Object.assign({}, firstChar.fields[3]);
                    let mesoFeild = Object.assign({}, firstChar.fields[4]);
                    let levelFeild = Object.assign({}, firstChar.fields[5]);


                    if (gm != `0`) {
                       // console.log(gm);
                        gmFeild.name = `Game Master`;
                        gmFeild.value = `GM Holder`;
                        if (gm == `1`) {gmFeild.value = `Donator`;}
                        if (gm == `2`) {gmFeild.value = `Intern`;}
                        if (gm == `3`) {gmFeild.value = `Game Master`;}
                        if (gm == `4`) {gmFeild.value = `Head Game Master`;}
                        if (gm >= `5`) {gmFeild.value = `Administrator`;}
                        fameFeild.inline = true;
                        charFeilds.push(gmFeild);
                        
                    }
                    

                    //let guildText = new Array;
                   // console.log(guild);

                    

                    //console.log(jobs.getJobs);

                    let thisJob = jobs.getJobs.find(obj => obj.id == job);
                   // console.log(thisJob.name);




                    guildFeild.name = `Guild`;
                    guildFeild.value = `${guildText}`;
                    guildFeild.inline = true;
                    JobFeild.name = `Class`;
                    JobFeild.value = `${thisJob.name}`;
                    JobFeild.inline = true;
                    levelFeild.name = `Level`;
                    levelFeild.value = `${level}`;
                    levelFeild.inline = true;
                    mesoFeild.name = `Mesos`;
                    mesoFeild.value = `${mesos}`;
                    mesoFeild.inline = true;
                    fameFeild.name = `Fame`;
                    fameFeild.value = `${fame}`;
                    fameFeild.inline = true;

                   // console.log(addEquip[0]);

                    //https://maplestory.io/api/character/{"itemId":47100},{"itemId":23086,"animationName":"default"},{"itemId":1051487},{"itemId":1070001},{"itemId":1302000},{"itemId":2000,"region":"GMS","version":"latest"},{"itemId":12000,"region":"GMS","version":"latest"}/stand1/animated?showears=false&showLefEars=false&showHighLefEars=false&resize=1&name=&flipX=false
                    firstChar.image = { // Hair / Eye
                        url: `https://maplestory.io/api/character/{"itemId":${hair}},{"itemId":${eyes},"animationName":"default"},${addEquip.toString()},{"itemId":2000,"region":"GMS","version":"latest"},{"itemId":12000,"region":"GMS","version":"latest"}/stand1/animated?showears=false&showLefEars=false&showHighLefEars=false&resize=1&name=&flipX=false`
                      }

                    charFeilds.push(guildFeild, JobFeild, levelFeild);


                    msg.channel.send(firstChar);


                });
                });



                } else {
                    msg.channel.send(`You don't have any characters!`);
                }


            });


            } else {
                msg.channel.send(`Please create an account by saying "register"`);
            }
        });
    }
}