const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');
 var newsql = require('../Brain/207Connect');
var sha1 = require('sha1');



module.exports.account = function(msg, discordID, randomThumb, args, celest) {

    if (msg.content.toLowerCase().includes(`account`)) {
        if (msg.channel.type == `dm`) {
            sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {

                if (results[0]) {
                let accRows = JSON.parse(JSON.stringify(results[0]));

                let username = accRows.name;
                let password = accRows.password;
                let email = accRows.email;
                
                //msg.channel.send(`Username: ${username}\rPassword: ${password}\r Email: ${email}`);

                let accountEmbed = new Discord.RichEmbed ({
                    title: `Your account info`,
                    fields: [
                        {
                            name: `Username`,
                            value: `${username}`,
                            inline: true,
                        },
                        {
                            name: `Password`,
                            value: `${password}`,
                            inline: true,
                        },
                        {
                            name: `Email`,
                            value: `${email}`,
                            inline: true,
                        }
                    ],
                    footer: {
                        text: `Want to change your info? Type "Edit".`,
                    }
                });

                msg.channel.send(accountEmbed);
                
            } else {
                msg.channel.send(`You don't have an account! say "Register" to sign up to celestialMS!`);
            }
            }); 
            
        }
    }

     if (msg.content.toLowerCase().includes(`edit207`)) {
        if (msg.channel.type == `dm`) {
            newsql.getResult(`SELECT * FROM users WHERE discordID = ${discordID}`, function(err, results) {

                if (results[0]) {

                    let editEmbed = new Discord.RichEmbed({
                        title: `Edit your account info!`,
                        fields: [
                            {
                                name: `Use this command like this:`,
                                value: `Edit username xxxx\rThis will change your username to xxxx`,
                            }
                        ],
                        footer: {
                            text: `You can view your current account info by saying "account"`,
                        }
                    });

                    if (!args[1] || !args[2]) {
                        msg.channel.send(editEmbed);
                        return;
                    }

                    let editType = args[1].toLowerCase();
                    let toEdit = args[2];

                    let test;

                    if (editType) {
                    
                   
                    switch(editType) {
                        case 'username':
                            newsql.getResult(`SELECT * FROM users WHERE name = '${toEdit}'`, function(err, results) {
                                if (results[0]) {
                                    editEmbed.fields[0] = {
                                        name: `${toEdit} is already taken!`,
                                        value: `Please try again!`,
                                    }
                                    msg.channel.send(editEmbed);
                                    return;
                                }
                                newsql.getResult(`UPDATE users SET name ='${toEdit}' WHERE discordID = ${discordID}`, function(err, results) {if (err) {console.log(err)}});
                                editEmbed.fields[0] = {
                                    name: `Your new Username is:`,
                                    value: `${toEdit}`,
                                }
                                msg.channel.send(editEmbed);
                            }); 
                               
                        break;
                        case 'password':
                                newsql.getResult(`UPDATE users SET password='${toEdit}' WHERE discordID = ${discordID}`, function(err, results) {if (err) {console.log(err)}});
                                editEmbed.fields[0] = {
                                    name: `Your new Password is:`,
                                    value: `${toEdit}`,
                                }
                                msg.channel.send(editEmbed);
                        break;
                        case 'email':
                            newsql.getResult(`UPDATE users SET email='${toEdit}' WHERE discordID = ${discordID}`, function(err, results) {if (err) {console.log(err)}});
                                editEmbed.fields[0] = {
                                    name: `Your new Email is:`,
                                    value: `${toEdit}`,
                                }
                                msg.channel.send(editEmbed);
                        break;

                        default: 
                            editEmbed.fields[0] = {
                                name: `Make sure you're trying to edit one of the following!`,
                                value: `Username / Password / Email`,
                            }
                            msg.channel.send(editEmbed);
                        break;

                    }
                }

                console.log(test);

                

                } else {
                    msg.channel.send(`You don't have an account! Type "register" to create one!`);
                }

            });
        }
    }


    if (msg.content.toLowerCase().includes(`edit`)) {
        if (msg.content.includes(`207`)) { return;}
        if (msg.channel.type == `dm`) {
            sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {

                if (results[0]) {

                    let editEmbed = new Discord.RichEmbed({
                        title: `Edit your account info!`,
                        fields: [
                            {
                                name: `Use this command like this:`,
                                value: `Edit username xxxx\rThis will change your username to xxxx`,
                            }
                        ],
                        footer: {
                            text: `You can view your current account info by saying "account"`,
                        }
                    });

                    if (!args[1] || !args[2]) {
                        msg.channel.send(editEmbed);
                        return;
                    }

                    let editType = args[1].toLowerCase();
                    let toEdit = args[2];

                    let test;

                    if (editType) {
                    
                   
                    switch(editType) {
                        case 'username':
                            sqlFunctions.getResult(`SELECT * FROM accounts WHERE name = '${toEdit}'`, function(err, results) {
                                if (results[0]) {
                                    editEmbed.fields[0] = {
                                        name: `${toEdit} is already taken!`,
                                        value: `Please try again!`,
                                    }
                                    msg.channel.send(editEmbed);
                                    return;
                                }
                                sqlFunctions.getResult(`UPDATE accounts SET name ='${toEdit}' WHERE discordID = ${discordID}`, function(err, results) {if (err) {console.log(err)}});
                                editEmbed.fields[0] = {
                                    name: `Your new Username is:`,
                                    value: `${toEdit}`,
                                }
                                msg.channel.send(editEmbed);
                            }); 
                               
                        break;
                        case 'password':
                                sqlFunctions.getResult(`UPDATE accounts SET password='${sha1(toEdit)}' WHERE discordID = ${discordID}`, function(err, results) {if (err) {console.log(err)}});
                                editEmbed.fields[0] = {
                                    name: `Your new Password is:`,
                                    value: `${toEdit}`,
                                }
                                msg.channel.send(editEmbed);
                        break;
                        case 'email':
                            sqlFunctions.getResult(`UPDATE accounts SET password='${toEdit}' WHERE discordID = ${discordID}`, function(err, results) {if (err) {console.log(err)}});
                                editEmbed.fields[0] = {
                                    name: `Your new Email is:`,
                                    value: `${toEdit}`,
                                }
                                msg.channel.send(editEmbed);
                        break;

                        default: 
                            editEmbed.fields[0] = {
                                name: `Make sure you're trying to edit one of the following!`,
                                value: `Username / Password / Email`,
                            }
                            msg.channel.send(editEmbed);
                        break;

                    }
                }

                console.log(test);

                

                } else {
                    msg.channel.send(`You don't have an account! Type "register" to create one!`);
                }

            });
        }
    }





    

}