const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');
var newsql = require('../Brain/207Connect');


module.exports.account = function(msg, discordID, randomThumb, args, celest) {


    if (msg.content.toLowerCase().includes(`link`)) {
        if (args[0].toLowerCase() !== `celest`) { return; }

        if (msg.channel.type !== `dm`) {
            msg.channel.send(`Please link your account in my DMs!`);
            return;
        }

          sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
            if (results[0]) {
                msg.channel.send(`Your account is already linked to discord!`);
            } else {
               if (!args[2]) {
                msg.channel.send(`Please Type "celest link accountName"`);
                return;
               }
               let accountLink = args[2];

               sqlFunctions.getResult(`SELECT * FROM accounts WHERE name = '${accountLink}'`, function(err, results) {
                let accRows = JSON.parse(JSON.stringify(results[0]));
                let discord = accRows.discordID;

                if (discord != 0) {
                    msg.channel.send(`This account is already linked to a discord!`);
                    return;
                } else {
                    sqlFunctions.getResult(`UPDATE accounts SET discordID = ${discordID} WHERE name = '${accountLink}'`, function (err, results){});
                    msg.channel.send(`Your account has been linked to discord! Type help to see a full list of commands you can use now that you're a member!`);
                    return;
                }
               });
        }
        });
    }

    if (msg.content.toLowerCase().includes(`unlink`)) {
        if (args[0].toLowerCase() !== `celest`) { return; }

        if (msg.channel.type !== `dm`) {
            msg.channel.send(`Please link your account in my DMs!`);
            return;
        }

          sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
            if (results[0]) {
                let accRows = JSON.parse(JSON.stringify(results[0]));

    
                sqlFunctions.getResult(`UPDATE accounts SET discordID = 0 WHERE discordID = ${discordID}`, function(err, results) {});
                newsql.getResult(`UPDATE users SET discordID = 0 WHERE discordID = ${discordID}`, function(err, results) {});
                
                   msg.channel.send(`Your account ${accRows.name} has been unlinked from this discord!`);
            } else {
                msg.channel.send(`You don't have an account linked to discord!`);
                return;
              
        }
        });
    }

}