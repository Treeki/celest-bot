const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');

module.exports.unstuck = function(msg, discordID) {
  sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
    if (results[0]) {
      var rows = JSON.parse(JSON.stringify(results[0]));
      console.log(`${msg.author.username} just Unstuck their account!`);
      sqlFunctions.getResult(`UPDATE accounts SET loggedin = 0 WHERE discordID = ${discordID}`, function(err, results) {});
      if (msg.content.includes(`unstuck`)) {
           msg.author.send(`I logged you out, try logging back in!`);
      }
    } else {
      msg.author.send(`Type "register" to set up an account!`);
    }
  });
}
