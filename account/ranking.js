const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


module.exports.doRanking = function(msg, discordID, randomThumb, args) {

  let type = `disMsg`;

  if (args[2]) {
    let rankingType = args[2];

    switch (rankingType) {
      case `vp`:
        type = `vpoints`;
      break;

      case `dp`:
        type = `dpoints`;
      break;

      case `mesos`:
        type = `mesos`;
      break;

      case `profit`:
        type = `profit`;
      break;

      default:
        type = `disMsg`;
      break;
    }
  }

  console.log(type);


  if (type == `mesos` || type == `profit`) {
    sqlFunctions.getResult(`SELECT * FROM bank ORDER BY ${type} DESC`, function(err, results) {

        let rankingByName = new Array;
        var amountDisplay = 0;
        let rankingFeilds = new Array;

        const rankingEmbed = new Discord.RichEmbed({
          title: `Hai ${msg.author.username}`,
          thumbnail: {
                url: randomThumb,
              },
          description: `Rankings for ${type} for CelestialMS\r\nTry saying a few others after ranking!\r\n vp / dp / mesos / profit`,
              fields: rankingFeilds
        });

        for (var i = 0; i < Object.keys(results).length; i++) {
          let rows = JSON.parse(JSON.stringify(results[amountDisplay]));
          let getAmount = rows.disMsg;
          let message = `Messages sent in Celestial!`;
          if (type == `mesos`) {getAmount = numberWithCommas(rows.mesos); message = `Mesos!`}
          if (type == `profit`) {getAmount = numberWithCommas(rows.profit); message = `Profit!`}
          //if (type == `vpoints`) {getAmount = rows.vpoints;}
          var amountDisplay = amountDisplay + 1;
          let pushRank = Object.assign({}, rankingEmbed.fields[amountDisplay]);

          rankingByName.push(rows.name);

          pushRank.name = `Rank #${amountDisplay}`;
          pushRank.value = `<@${rows.discordID}>, They have ${getAmount} ${message}`;
          rankingFeilds.push(pushRank);


          //console.log(amountDisplay);
          //console.log(rows.name);
          if (amountDisplay == 5) {
            msg.channel.send(rankingEmbed);
            }
          }
          return;
    });
  } else {




  sqlFunctions.getResult(`SELECT * FROM accounts ORDER BY ${type} DESC`, function (err, results) {


      let rankingByName = new Array;
      var amountDisplay = 0;
      let rankingFeilds = new Array;

      const rankingEmbed = new Discord.RichEmbed({
        title: `Hai ${msg.author.username}`,
        thumbnail: {
              url: randomThumb,
            },
        description: `Rankings for ${type} for CelestialMS\r\nTry saying a few others after ranking!\r\n vp / dp / mesos / profit`,
            fields: rankingFeilds
      });

  for (var i = 0; i < Object.keys(results).length; i++) {
    let rows = JSON.parse(JSON.stringify(results[amountDisplay]));
    let getAmount = rows.disMsg;
    let message = `Messages sent in Celestial!`;
    if (type == `vpoints`) {getAmount = rows.vpoints; message = `Vote points!`}
    if (type == `dpoints`) {getAmount = rows.dpoints; message = `Donor Points!`}
    //if (type == `vpoints`) {getAmount = rows.vpoints;}
    var amountDisplay = amountDisplay + 1;
    let pushRank = Object.assign({}, rankingEmbed.fields[amountDisplay]);

    rankingByName.push(rows.name);

    pushRank.name = `Rank #${amountDisplay}`;
    pushRank.value = `<@${rows.discordID}>, They have ${getAmount} ${message}`;
    rankingFeilds.push(pushRank);


    //console.log(amountDisplay);
    //console.log(rows.name);
    if (amountDisplay == 5) {
      msg.channel.send(rankingEmbed);
      }
    }

  //console.log(Object.keys(results).length);
});
}
}
