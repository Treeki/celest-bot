const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


module.exports.doGive = function(msg, discordID, randomThumb, args) {
  if (args[0] !== `celest`) {
    if (args[0] !== `Celest`) {
      return;
    }
  }
  sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = '${discordID}'`, function (err, results) {
  if (err) {

  }
  if (results[0]) {
    let rows = JSON.parse(JSON.stringify(results[0]));

    //let maxMeso = rows.mesos; Mesos aren't stored in accounts silly
    let maxVp = rows.vpoints;
    let maxDp = rows.dpoints;

    let typeToGive = `mesos`;

    let giveAmount = args[3];
    let playerToGive;
    if (args[2]) {
       playerToGive = args[2].replace(/[&\/\\#,@+()$!~%.'":*?<>{}]/g, '');
    }
    let playerToGiveLiteral = args[2];

    let type = `mesos`;
    let thisTable = `bank`;

    if (args[4]) {
      let giveType = args[4];
  
      switch (giveType) {
        case `vp`:
          type = `vpoints`;
          thisTable = `accounts`;
        break;
  
        case `dp`:
          type = `dpoints`;
          thisTable = `accounts`;
        break;
  
        default:
          type = `mesos`;
          thisTable = `bank`;
        break;
      }
    }


    let giveFeilds = new Array;

    const giveEmbed = new Discord.RichEmbed({
      title: `Pending Transaction...`,
      thumbnail: {
            url: randomThumb,
          },
          fields: giveFeilds
    });

    let pushGive = Object.assign({}, giveEmbed.fields[0]);

    console.log(giveAmount);
    console.log(playerToGive);

    if (args[2] == undefined) {
      pushGive.name = `You didn't tell me who to give to!`;
      pushGive.value = `Example: \n Celest give @Taiga 1000000 mesos`;
      giveFeilds.push(pushGive);
      msg.channel.send(giveEmbed);
      return;
    }
    if (args[3] == undefined) {
      pushGive.name = `You didn't tell me how much to give!`;
      pushGive.value = `Example: \n Celest give @Taiga 1000000 mesos`;
      giveFeilds.push(pushGive);
      msg.channel.send(giveEmbed);
      return;
    }

    if (!isNaN(giveAmount)) {
      console.log(`${msg.author.username} is attempting to give ${giveAmount} mesos`);
    } else {
      //console.log('is not a number!');
      pushGive.name = `Are you sure ${giveAmount} is a whole number?`;
      pushGive.value = `Example: \n Celest give @Taiga 1000000 mesos`;
      giveFeilds.push(pushGive);
      msg.channel.send(giveEmbed);
      return;
    }

    playerToGive = playerToGiveLiteral.replace(/[&\/\\#!,@+()$~%.'":*?<>{}]/g, '');
    if (!isNaN(playerToGive)) {
      console.log(`${msg.author.username} is attempting to give ${playerToGiveLiteral} mesos`);
    } else {
      //console.log('is not a number!');
      pushGive.name = `Are you sure ${playerToGiveLiteral} is an actual person?`;
      pushGive.value = `Example: \n Celest give @Taiga 1000000 mesos \n make sure you tag them!`;
      giveFeilds.push(pushGive);
      msg.channel.send(giveEmbed);
      return;
    }

    if (`${playerToGive}` !== `${discordID}`) {
      console.log(`${playerToGive} !== ${discordID}`);
    } else {
      //console.log('is not a number!');
      pushGive.name = `I know you're lonely but come on.`;
      pushGive.value = `Example: \n Celest give @Taiga 1000000 mesos \n make sure you tag them!`;
      giveFeilds.push(pushGive);
      msg.channel.send(giveEmbed);
      return;
    }

    if (msg.content.includes(`-`)) {
      pushGive.name = `Why are you trying to break me, do this instead.`;
      pushGive.value = `Example: \n Celest give @Taiga 1000000 mesos`;
      giveFeilds.push(pushGive);
      msg.channel.send(giveEmbed);
      return;
    }


      sqlFunctions.getResult(`SELECT ${type} FROM ${thisTable} WHERE discordID = ${discordID}`, function (err, results) {
        if (results[0]) {
        var rowsmeso = JSON.parse(JSON.stringify(results[0]));
        //console.log(playerToGive);
        //console.log(playerToGiveLiteral);

        let amount;
        if (type == `mesos`) {
          amount = rowsmeso.mesos;
        }
        if (type == `vpoints`) {
          amount = rowsmeso.vpoints;
        }

        if (type == `dpoints`) {
          amount = rowsmeso.dpoints;
        }
        


      if (amount < giveAmount) {
        pushGive.name = `Are you sure you have that many ${type}?`;
        pushGive.value = `Your bank only has: \n ${amount}`;
        giveFeilds.push(pushGive);
        msg.channel.send(giveEmbed);
        return;
      } else {
        sqlFunctions.getResult(`SELECT * FROM ${thisTable} WHERE discordID = '${playerToGive}'`, function (err, results) {
          var rowsmeso2 = JSON.parse(JSON.stringify(results[0]));

          if (results[0]) {
            sqlFunctions.getResult(`UPDATE ${thisTable} SET ${type} = ${type} - ${giveAmount} WHERE discordID = '${discordID}'`, function(err, results) {

            });
            sqlFunctions.getResult(`UPDATE ${thisTable} SET ${type} = ${type} + ${giveAmount} WHERE discordID = '${playerToGive}'`,function(err, results) {

            });
            pushGive.name = `The kindness of ur heart warms me`;
            console.log(type);
            let selfValue;
            let giftValue;
            if (type == `mesos`) {
               selfValue = numberWithCommas(rowsmeso.mesos - giveAmount);
               giftValue = numberWithCommas(Number(rowsmeso2.mesos) + Number(giveAmount));
            }
            if (type == `vpoints`) {
               selfValue = numberWithCommas(rowsmeso.vpoints - giveAmount);
               giftValue = numberWithCommas(Number(rowsmeso2.vpoints) + Number(giveAmount));
            }
    
            if (type == `dpoints`) {
               selfValue = numberWithCommas(rowsmeso.dpoints - giveAmount);
               giftValue = numberWithCommas(Number(rowsmeso2.dpoints) + Number(giveAmount));
            }
            pushGive.value = `<@${discordID}> now has ${selfValue} ${type} \n ${playerToGiveLiteral} now has ${giftValue} ${type}`;
            giveEmbed.title = `Transaction Completed!`;
            giveFeilds.push(pushGive);
            //msg.delete();
            msg.channel.send(giveEmbed);
            return;
          } else {
            pushGive.name = `Something doesn't seem right to me...`;
            pushGive.value = `are you sure ${playerToGiveLiteral} has an account with me?`;
            giveFeilds.push(pushGive);
            msg.channel.send(giveEmbed);
            return;
          }

      });
      }
    } else {
      msg.channel.send(`I did an Oopsie, try again uwu`);
      return;
    }
    });



    //msg.channel.send(giveEmbed);
    return;
  } else {
    msg.author.send(`You don't have an account! type "register" and i'l help you out!`);
    return;
  }
});
}
