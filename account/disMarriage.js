const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');



module.exports.doMariage = function(msg, discordID, randomThumb, args) {

  if(msg.content.toLowerCase().includes(`marry`)) {

    if (args[0] !== `marry`) {
      return;
    }
    sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
      if (results[0]) {

        var rows = JSON.parse(JSON.stringify(results[0]));

        if (!args[1]) {
          let marryEmbed = new Discord.RichEmbed({
            title: `<a:Celest:610021151820873738>Marry Commands<a:Celest:610021151820873738>`,
            fields: [
              {
                name: `Marry`,
                value: `This will send a proposal To the person you want to Marry\rExample: Marry @taiga`,
                inline: true,
              },
              {
                name: `Accept`,
                value: `Just say the word accept after you have a proposal to accept it!`,
                inline: true,
              },
              {
                name: `Decline`,
                value: `Just say the word Decline after a proposal to reject the person!`,
                inline: true,
              },
              {
                name: `Divorce`,
                value: `Say the word Divorce if you would like to end a marriage!`,
                inline: true,
              },
            ]
          });
          msg.channel.send(marryEmbed);
          return;
        }

        let personToMarry = args[1].replace(/[&\/\\#,@+()!$~%.'":*?<>{}]/g, '');
        let marriedTo = rows.disMarry;
        let isPending = rows.disPending;

        console.log(marriedTo);
        console.log(isPending);


        if (!personToMarry) {
          msg.channel.send(`You didn't tell me who you wanted to marry!`);
          return;
        }

        if (personToMarry == discordID) {
          msg.channel.send(`I know you're lonely but you can't marry urself`);
          return;
        }

        if (isPending != 0) {
          msg.channel.send(`You already have a Pending Proposal from/to <@${isPending}>!`);
          return;
        }

        if (marriedTo == 0) {
          sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${personToMarry}`, function(err, results) {
            if (results[0]) {
              let checkRow = JSON.parse(JSON.stringify(results[0]));

              let otherMarried = checkRow.disMarry;
              if (otherMarried == 0) {


              let otherPending = checkRow.disPending;
              console.log(otherPending);
              if (otherPending == 0) {
                msg.channel.send(`I sent a Proposal Request to <@${personToMarry}>\rThey need to say either "Accept or Reject"!`);
                sqlFunctions.getResult(`UPDATE accounts SET disPending = '${discordID}' WHERE discordID = '${personToMarry}'`, function(err, results) {});
                //sqlFunctions.getResult(`UPDATE accounts SET disPending = '${personToMarry}' WHERE discordID = '${discordID}'`, function(err, results) {});
                return;
              } else {
                msg.channel.send(`this person already has a pending proposal from <@${otherPending}>`);
                return;
              }
            } else {
                msg.channel.send(`This person is already married to <@${checkRow.disMarry}>!`);
                return;
              }
          } else {
              msg.channel.send(`This person doesn't have an account with me!`);
              return;
            }

          });

        } else {
          msg.channel.send(`You're already married to <@${rows.disMarry}>!`);
        }

      } else {
        msg.channel.send(`You don't have an account!`);
        return;
      }
    });
  }


  if (msg.content.toLowerCase().includes(`accept`)) {
    sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
      if (results[0]) {

        var rows = JSON.parse(JSON.stringify(results[0]));

        let marriedTo = rows.disMarry;
        let isPending = rows.disPending;

        if (isPending != 0) {
          msg.channel.send(`You accepted a proposal from <@${isPending}>! I hope you guys the best wishes uwu`);
          sqlFunctions.getResult(`UPDATE accounts SET disPending = '0', disMarry = '${isPending}' WHERE discordID = '${discordID}'`, function(err, results) {});
          sqlFunctions.getResult(`UPDATE accounts SET disPending = '0', disMarry = '${discordID}' WHERE discordID = '${isPending}'`, function(err, results) {});
          return;
        }
        return;

      }

      });
  }

  if (msg.content.toLowerCase().includes(`reject`) || msg.content.toLowerCase().includes(`decline`)) {
    sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
      if (results[0]) {

        var rows = JSON.parse(JSON.stringify(results[0]));

        let marriedTo = rows.disMarry;
        let isPending = rows.disPending;

        if (isPending != 0) {
          msg.channel.send(`You Rejected a proposal from <@${isPending}>! That's prety depressing\rsad uwu`);
          sqlFunctions.getResult(`UPDATE accounts SET disPending = '0' WHERE discordID = '${discordID}'`, function(err, results) {});
          sqlFunctions.getResult(`UPDATE accounts SET disPending = '0' WHERE discordID = '${isPending}'`, function(err, results) {});
          return;
        } else {
          console.log(`What?`);
        }
        return;

      }

      });

    }

    if (msg.content.toLowerCase().includes(`divorce`)) {
      sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
        if (results[0]) {

          if (!args[1]) {
            msg.channel.send(`Make sure you say "Divorce yes"!\rSad to sea couple break up`);
            return;
          }

          var rows = JSON.parse(JSON.stringify(results[0]));

          let marriedTo = rows.disMarry;
          let isPending = rows.disPending;

          if (marriedTo != 0 && args[1] === `yes`) {
            msg.channel.send(`You divorced <@${marriedTo}>\rSorry it had to happen :C`);
            sqlFunctions.getResult(`UPDATE accounts SET disMarry = '0' WHERE discordID = '${discordID}'`, function(err, results) {});
            sqlFunctions.getResult(`UPDATE accounts SET disMarry = '0' WHERE discordID = '${marriedTo}'`, function(err, results) {});
            return;
          } else {
            console.log(`What?`);
          }
          return;

        }

        });

      }
  }
