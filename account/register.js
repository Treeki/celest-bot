const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');
var sha1 = require('sha1');

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


module.exports.doRegister = function(msg, discordID, randomThumb, args, celest) {

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}


      const embed = new Discord.RichEmbed({
        title: 'Your account info will be shown below',
        thumbnail: {
              url: randomThumb,
            },
        description: 'Type the credentials in order as they appear top to bottom, so type your username first, watch it update in my box, then type your password.',
            fields: [
            {
              name: `What would you like your username to be?`,
              value: 'Example: Celest',
            },
            {
              name: `Password:`,
              value: `Example: Password123`,
            },
            {
              name: `Email:`,
              value: `Example: Celest@celest.com`,
            },
            {
              name: `Birthday:`,
              value: `Example: 12-12-1996`,
            },
            {
              name: `Is this info Correct?`,
              value: `Example: Yes / No`,
            }
        ]
      });


    var msgId = new Array;

    msg.channel.send(embed).then(sent => {
      msgId.push(sent.id);
    });

    var messages = new Array;


    const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 900000 });
    collector.on('collect', msg => {

      //var messageArray = Array.from(collection.values());
        sqlFunctions.getResult('SELECT * FROM accounts WHERE discordID = ' + discordID + '', function (err, results) {
          if(results[0]) {
            var rows = JSON.parse(JSON.stringify(results[0]));
            return msg.channel.send(`you already have an account, your username is ${rows.name} silly`);
          } else {

            if (msg.content.toLowerCase().includes(`cancel`)) {
              collector.stop();
              msg.channel.send(`Registration Cancled!`);
              return;
              
            }

            messages.push(msg.content);



            let username = messages[0];
            let password = messages[1];
            let email    = messages[2];
            let dob      = messages[3];

            let usernameEmbed = Object.assign({}, embed.fields[0]);
            let passwordEmbed = Object.assign({}, embed.fields[1]);
            let emailEmbed = Object.assign({}, embed.fields[2]);
            let birthdayEmbed = Object.assign({}, embed.fields[3]);
            let confirmEmbed = Object.assign({}, embed.fields[4]);

            let selector = new Array;

            



              if (messages[0]) { // username logic

                usernameEmbed.name =  `Your Username Will be:`;
                usernameEmbed.value = `${messages[0]}`;
                selector.push(usernameEmbed);
                passwordEmbed.name = `What would you like your password to be?`;
                selector.push(passwordEmbed);



                if (messages[1]) { //pasword logic

                  passwordEmbed.name = `Your Password will be:`;
                  passwordEmbed.value = `${messages[1].length} characters long`;
                  emailEmbed.name = `What would you like your email to be?`;
                  selector.push(emailEmbed);
                  console.log(selector);
                }

                if (messages[2]) { //email logic

                  sqlFunctions.getResult(`SELECT * FROM accounts WHERE email = '${email}'`, function (err, results) {
                    //if (error) throw (error);
                    if (results[0]) {
                      var rows = JSON.parse(JSON.stringify(results[0]));
                      emailEmbed.name = `Email already in use!`;
                      emailEmbed.value = `Contact an admin to see if the account is yours!`;
                      messages.pop(messages[2]);
                    } else {
                      if (!messages[3]) {
                        if (validateEmail(email) == false) {
                          emailEmbed.name = `Your email isn't vaild!`;
                          emailEmbed.value = `Are you sure ${email} is a vaild email? try again!`;
                          messages.pop(messages[2]);
                        } else {
                        emailEmbed.name = `Your Email will be:`;
                        emailEmbed.value = `${messages[2]}`
                        birthdayEmbed.name = `What would you like your Birthday to be?`;
                        selector.push(birthdayEmbed);

                        }
                      }

                    }

                  });


                }
                if (messages[3]) { //birthday logic
                  birthdayEmbed.name = `Your Birthday will be:`;
                  birthdayEmbed.value = `${messages[3]}`;

                  confirmEmbed.name = `Is this information correct?`;
                  confirmEmbed.value = `Yes / No`;
                  selector = [];
                  selector.push(usernameEmbed, passwordEmbed, emailEmbed, birthdayEmbed);
                  selector.push(confirmEmbed);

                }
                if (messages[4]) { //confirm logic

                  var createAccount = messages[4];
                  selector = [];

                  if (createAccount === 'yes' || createAccount === 'Yes') {
                    sqlFunctions.getResult(`INSERT INTO accounts (name, password, email, birthday, discordID) VALUES ('${username}', '${sha1(password)}', '${email}', '${dob}', '${discordID}')`,function(err, results) {
                    confirmEmbed.name = `I'll start counting every message you send in the server and track all of your in game progress!`;
                    confirmEmbed.value = `Thanks for joining Celestial, Keep posted for updates, i'll let you know of any uwu`;
                    
                    selector.push(confirmEmbed);
                    collector.stop();
                
                  });
                } else {
                  confirmEmbed.name = `Okay try running the command again! uwu`;
                  confirmEmbed.value = `Once you have an acount you won't beable to delete it or make another until taiga makes a delete command for me <3`;
                  selector.push(confirmEmbed);
                  collector.stop();
                }
              }

              } else {
                usernameEmbed.name = `Some error happened`;
                usernameEmbed.value = `sowwy`;
                collector.stop();
              }




              // update 'field' with new value

              // create new embed with old title & description, new field
              const newEmbed = new Discord.RichEmbed({
                title: embed.title,
                thumbnail: embed.thumbnail,
                //description: `Answer my questons below and we'll get you started!`,
                fields: selector,
                footer: {
                  text: `Type "Cancel" at any time to end the registration and start again!`,
                }
              });

              console.log(msg.channel.fetchMessages({around: msgId, limit: 1}))

              msg.channel.fetchMessages({around: msgId, limit: 1})
                .then(msg => {
                    const fetchedMsg = msg.first();
                    fetchedMsg.edit(newEmbed);
                });
              // edit message with new embed
              // NOTE: can only edit messages you author
              //msg.edit(newEmbed)
                //.then(newMsg => console.log(`new embed added`))
                //.catch(console.log);



          }
        });
  });

  collector.on('end', collected => {
    //Write actual code to send message to use if it timed out;
    console.log(`Finished Making account`);
  });
}
