const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');
var newsql = require('../Brain/207Connect');
const bcrypt = require('bcrypt');
const saltRounds = 10;


module.exports.account = function(msg, discordID, randomThumb, args, celest) {

    if (msg.content.toLowerCase().includes(`upgrade 207`)) {

        let embed207 = new Discord.RichEmbed({
            title: 'Upgrade your Celestial account!',
            fields: [
                {
                    name: `Before you upgrade We're going to need you to reset your password`,
                    value: `The reason for this is we are now using BCRYPT Over Sha512, this is extreme Enhanced security for your protection.\r\n
                    What would you like your nnew pasword to be?`,
                }
            ]
        });

        if (msg.channel.type !== `dm`) {
            msg.channel.send("You must Upgrade in my DMs!");
            return;
        }

        let confirm = args[2];

        if (confirm != 'yes') {
            embed207.title = `<a:MegaShout:611347105973272576>WARNING<a:MegaShout:611347105973272576>`;
            embed207.fields[0] = {
                name: `BEFORE YOU UPGRADE READ BELOW!`,
                value: `
                When you upgrade your account you will be losing features:\r\n
                THIS UPGRADE IS MAINLY FOR FOUNDER PACK HOLDERS!!`, 
            }
            embed207.fields[1] = {
                name: `So what does this do?`,
                value: `When you upgrade it ports your account over from v179 to v207\r\n
                Accounts are handled differently in this version, so alot of the bot commands WILL NOT WORK after you do this!\r\n
                Doing this will allow you to sign into the v207 client, HOWEVER, If you are not a founder pack owner, this will do nothing for you.`, 
            }
            embed207.fields[2] = {
                name: `Okay I'm ready, now what?`,
                value: `In order to start the process of upgrading your account from 179 > 207 you must first say\r\n"upgrade 207 yes" Exactly like that.`, 
            }
            msg.channel.send(embed207); 
            return;
        }

       sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
        if (results[0]) {

            let accRows = JSON.parse(JSON.stringify(results[0]));

            let username = accRows.name;
            let password = accRows.password;
            let vpoints = accRows.vpoints;
            let founder = accRows.founder;
            let email = accRows.email;
            let disMsg = accRows.disMsg;
            let discLevel = accRows.discLevel;
            let disMarry = accRows.disMarry;
            let disPending = accRows.disPending;

            if (founder == 0) {
                embed207.fields[0] = {
                    name: `Hello ${msg.author.name}`,
                    value: `Upgrading accounts for players without a founders pack is currently prohibited`, 
                }
                embed207.fields[0] = {
                    name: `Want to Reserve an IGN?`,
                    value: `Purchase a founders pack by [clicking here](http://celestialms.com)`, 
                }
                msg.channel.send(embed207);
                return;
            }

            var msgId = new Array;
    
                msg.channel.send(embed207).then(sent => {
                msgId.push(sent.id);
                });
            
                var messages = new Array;


            const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 9000000 });
            collector.on('collect', msg => {

                let newpass;

            if (msg.content.toLowerCase().includes(`cancel`)) {
                msg.channel.send("Account upgrade has been canceled");
                collector.stop();
                return;
            }

            messages.push(msg.content);
            console.log(messages);

            if (messages[0]) {
                embed207.fields[0] = {
                    name: `Okay your new password has been set, are you sure you wish to contuine?`,
                    value: `Please say "Yes" Otherwise the Upgrade will be cancled.`,
                } 
                newpass = messages[0];
            }

            if (messages[1]) {
                if (messages[1] != "Yes") {
                    msg.channel.send("Account upgrade has been Cancled");
                    collector.stop();
                    return;
                } 
                embed207.fields[0] = {
                    name: `Your account has ben upgraded!`,
                    value: `Most of Celests Features will be unaviable to you until everyone can update their accounts to 207`,
                }

                newsql.getResult(`SELECT * FROM users WHERE discordID = ${discordID}`, function(err, results) {
                    if (results[0]) {
                        msg.channel.send("You have already upgraded your account!");
                        console.log(results[0]);
                        collector.stop();
                        return;
                    } else {
                    bcrypt.genSalt(saltRounds, function(err, salt) {
                        bcrypt.hash(newpass, salt, function(err, hash) {
                            let newPassHash = hash;
                            console.log(`${username}, ${newpass}, ${vpoints}, 0, ${founder}, ${discordID}, ${disMsg}, ${discLevel}, ${disMarry}, ${disPending}`);
                            newsql.getResult(`INSERT INTO users
                            (name, password, votepoints, accounttype, founder, email, discordID, disMsg, discLevel, disMarry, disPending) 
                            VALUES 
                            ('${username}', '${newpass}', ${vpoints}, 0, ${founder}, '${email}', ${discordID}, ${disMsg}, ${discLevel}, '${disMarry}', '${disPending}')`, function(err, results) {
                                if(err) {
                                    console.log(err);
                                } 
                                console.log(results);
                            });
                        });
                    });
                }
                });

                //Upgrade acount here 
                
            }

            const newEmbed = new Discord.RichEmbed({
                title: embed207.title,
                fields: embed207.fields,
                footer: {
                    text: `Please wait for my text box to change before typing anything else!`,
                }
              });

              console.log(msg.channel.fetchMessages({around: msgId, limit: 1}))

              msg.channel.fetchMessages({around: msgId, limit: 1})
                .then(msg => {
                    const fetchedMsg = msg.first();
                    fetchedMsg.edit(newEmbed);
                });
      



        });



        } else {
            msg.channel.send(`You don't even have a v179 account, If you wish to create a v207 acount, please register on the Website!\r\n
            [Click here to Register for 207](http://celestialms.com)`);
        }
       }); 

    }

}