module.exports = {
  msgChannel: [
    `Hewwo i sent you a spicy pm 🔥`,
    `i just hit ya with a dm`,
    `you looks really cute uwu so i slide into ur dms`,
    `you want to know somethin? this info is a lil personal so i'll put it in your PMs`,
    `that info is a lil personl so i'll slide it under the table`,
    `i'm a litle embarrassed to say it in public, i'll just put it in your mailbox :c`,
    `your asking so much from me D: but i suppose i can still send it to you in private`,
    `uwu lets talk about that in private`,
    `i don't think we should be doin that in public sir`,
  ],

  embedImg: [
    `https://cdn.shopify.com/s/files/1/2786/5396/products/v7mJio7_300x300.png?v=1516245054`,
    `https://orangemushroom.files.wordpress.com/2017/09/maplestory-256x256.png`,
    `https://4.bp.blogspot.com/-a5j8JOMlzhU/Wc0FTWlS0FI/AAAAAAAKuDw/BnWF8Jgq0ocv4IbIVR3ye35BFySyI_5swCLcBGAs/s1600/AW555308_04.gif`,
    `https://1.bp.blogspot.com/-aNk2mQaegLg/Wc0FS1ORNrI/AAAAAAAKuDo/jefWNjKSSnIGv6dHjphDrhPxzSed05hrQCLcBGAs/s1600/AW555308_02.gif`,
    `https://1.bp.blogspot.com/-ei7ZPnRQJXM/WyjUtMX29iI/AAAAAAAgUYw/pifQDYU_EcY0SyhHAWUfxEW6jZ91h0wbACLcBGAs/s1600/AW1236833_14.gif`,
    `http://media.playpark.net/MapleStory/uploadimages/PBG4.PNG`,
    `https://4.bp.blogspot.com/-_88rCHn88Jw/WyjUwMRb7VI/AAAAAAAgUZU/_eH53g8Kbpg5X2i3-6zlm8c1u33hYRjVQCLcBGAs/s1600/AW1236833_23.gif`,
  ],

  noCommand: [
    `hewwo, i dun't understand what ur trying to say to me and it's making me a big sad`,
    `whatever ur saying makes no sense to me`,
    `can you try saying something i can actually understand u plebian`,
    `im gonna need some more help from taiga to understand that one`,
    `idunno what ur gettin at`,
    `no comprehendo wtf ur tryin to say to me`,
    `i wish i could understand you but your intelligence is just so low`,
    `i don't know what ur trying to say to me and it scares me`,
    `i love you but i don't understand you`,
    `im just lonely and what ur saying confuses me`,
    `hewwo what did u want?`,
    `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQm6lu-h9QuUyPcap7DCTK8L-f5tCDAYfWWik7oC2FRF8b4XqZc`,
    `https://i.gifer.com/X0hM.gif`,
    `https://media1.tenor.com/images/5e5507a6ec490f07864b86aff7e32852/tenor.gif?itemid=13802683`,
  ],

  animeRecco: [
    `You should watch Toradora! This will definalty make you do a big cry`,
    `What about watching Masamunes Revenge? The main character is a bit dicky but it's v funny uwu`,
    `Need a good romance anime? Watch Chunnibyo! Rikka is the cutest litle button i've ever seen oml.`,
    `Depends on what ur in the mood for, i'm in the mood to watch yuno go crazy, watch Future Diary <3`,
    `If you like a little bit of ecchi, (just a little), go watch Monster Musume`,
    `I'm sure you've seen it, but you should watch Re:zero (again) and forget who rem is?`,
    `don't hate on me, but you should watch sword art online, the new season 3 is by far the best <3`,
    `some generic iseki here, but you could give "In another world with my smartphone" a try, it's big funny`,
    `If you need a trip in some feels, watch kokoro connect, gets v intense`,
    `this is a staple in all animes, go watch Heavens Lost Propety`,
    `I need to fill my brain with more! Suggest replies for me in #suggestions`,
  ],

  loveReply: [
    `I gues I love you too..`,
    `I might beable to love you..`,
    `no ur ugly`,
    `i love/hate you but thats okay bc ur a little special`,
    `i onwe luf taiga`,
    `idk what u want from me but i don't love you`,
    `i love you too sometimes, don't abuse me :C`,
    `your heart is a lie, i dunt love you`,
    `you make my heart flutter`,
    `i get a wittle bit of buterflies when u talk to me`,
    `i know you love me`,
  ],

  hate: [
    `i hate you too.. baka`,
    `you hate me??.`,
    `no ur ugly`,
    `i wish i hated myself but i can't, im smarter than u `,
    `good i don't need u to like me i only love taiga`,
    `why would u hate me, hello??`,
    `where do u get of saying u hate me`,
    `sometimes i hate people a litle to`,
    `i cant wait til u die`,
    `u r a big gay, dont hate me bc u cant love urself`,
    `i know you love me`,
  ],

  randomItemCommon: [
    {
      itemID: `1042312`,
      name: `Blue Mushroom TShirt`,
      desc: `Cute ass shirt with some hearts around it`,
      type: `top`,
      rarity: `1`
    },
    {
      itemID: `1102919`,
      name: `Tag-Along baby ducky`,
      desc: `Cute duck train to follow you around`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102910`,
      name: `Smile Seed Cape`,
      desc: `Cape that smiles and has some seeds`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1012008`,
      name: `Cencor`,
      desc: `Cover your face!`,
      type: `faceAcc`,
      rarity: `1`
    },
    {
      itemID: `1053084`,
      name: `Violet Cube Outfit`,
      desc: `What even is this?`,
      type: `overall`,
      rarity: `1`
    },
    {
      itemID: `1702347`,
      name: `Fortune Flash`,
      desc: `What even is this?`,
      type: `weapon`,
      rarity: `1`
    },
    {
      itemID: `1702336`,
      name: `Lord Tempest`,
      desc: `Big Weeb Equals Big Weapon`,
      type: `weapon`,
      rarity: `1`
    },
    {
      itemID: `1102077`,
      name: `Cotton Blanket`,
      desc: `Feel's so warm!`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102243`,
      name: `Dunas Cape`,
      desc: `Dun as this cape.`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102644`,
      name: `Pretty Pixie`,
      desc: `Lookin like Tinker Bell!`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102667`,
      name: `Magical Misty Moon`,
      desc: `Water Type Leader`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102700`,
      name: `Petit Ciel`,
      desc: `Piccolo kinda OD`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102758`,
      name: `Victory Wings`,
      desc: `Can't relate`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102787`,
      name: `Scout Regiment Cape`,
      desc: `Titans, are kinda hot.`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1042357`,
      name: `Cloud Prison`,
      desc: `biggest hene hoe item`,
      type: `top`,
      rarity: `1`
    },
    {
      itemID: `1102827`,
      name: `The Kingdom Cape of King`,
      desc: `King is the worst piece in chess`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102831`,
      name: `Soaring High`,
      desc: `You can't fly silly.`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102844`,
      name: `Mousy Bunny Bouncy Buddies`,
      desc: `The name of this item is cancer`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1102747`,
      name: `Cutie Pandas`,
      desc: `Pandas are OD`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1012055`,
      name: `Allergic Reaction`,
      desc: `Its just dust uwu`,
      type: `faceAcc`,
      rarity: `1`
    },
    {
      itemID: `1062247`,
      name: `Knee Freedom`,
      desc: `Big Boy Pants`,
      type: `pants`,
      rarity: `1`
    },
    {
      itemID: `1001002`,
      name: `Witch Hat`,
      desc: `Burn her!`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1000010`,
      name: `Blue M-Forcer Helmet`,
      desc: `MANGO!`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1000011`,
      name: `Green M-Forcer Helmet`,
      desc: `only red ranger matters`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1001014`,
      name: `Pink M-Forcer Helmet`,
      desc: `There's a pink one?`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1001015`,
      name: `Yellow M-Forcer Helmet`,
      desc: `Yellow Fever is OD cute`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1001016`,
      name: `Black M-Forcer Helmet`,
      desc: `KFC`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1042358`,
      name: `Soft Olive Knitwear`,
      desc: `You are what you eat!`,
      type: `top`,
      rarity: `1`
    },
    {
      itemID: `1102748`,
      name: `Rabbit-Bear Camping Bag`,
      desc: `What's a Rabbit-Bear? O.o`,
      type: `cape`,
      rarity: `1`
    },
    {
      itemID: `1003883`,
      name: `Blue Bow Beret`,
      desc: `Do you hear the trees speaking?`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1061110`,
      name: `Pink Training Shorts`,
      desc: `Poor mans underpants. :(`,
      type: `bottom`,
      rarity: `1`
    },
    {
      itemID: `1051442`,
      name: `Momo Maid Dress`,
      desc: `Servant's clothing`,
      type: `overall`,
      rarity: `1`
    },
    
{
  itemID: `1073168`,
  name: `Chained Princess Shoes`,
  desc: `Ball and fetter of the defeated kingdom's princess`,
  type: `shoes`,
  rarity: `1`
},
{
  itemID: `1053059`,
  name: `Villan's Cool Tights`,
  desc: `Latex bondage suit`,
  type: `overall`,
  rarity: `1`
},

{
  itemID: `1052225`,
  name: `Lolita Butterfly Dress`,
  desc: `Casual wear belonging to an underage girl`,
  type: `overall`,
  rarity: `1`
},
{
  itemID: `1052032`,
  name: `Red Bruma`,
  desc: `Furious girl's fitness wear`,
  type: `overall`,
  rarity: `1`
},
{
  itemID: `1052033`,
  name: `Green Bruma`,
  desc: `Joyful girl's fitness wear`,
  type: `overall`,
  rarity: `1`
},
{
  itemID: `1052034`,
  name: `Blue Bruma`,
  desc: `Distraught girl's fitness wear`,
  type: `overall`,
  rarity: `1`
},
{
  itemID: `1702867`,
  name: `Fairy in a Gilded Cage`,
  desc: `Let TINKER BELL GO!`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702860`,
  name: `Starry Summer Night Weapon`,
  desc: `like that nostalgic summer`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1071090`,
  name: `Bloody Heels`,
  desc: `The wedding shoes of a maiden forced into confinement`,
  type: `shoes`,
  rarity: `1`
},
{
  itemID: `1702882`,
  name: `Snowflake Sugarpop`,
  desc: `You must live in Canada!`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702779`,
  name: `Fluffy Teddy Candy`,
  desc: `a description do give shoutouts to @Mico`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702767`,
  name: `Mustachio on a Stick`,
  desc: `TRASH`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702729`,
  name: `Deep-fried Drumstick`,
  desc: `11 herbs and spices or Popeyes?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702759`,
  name: `Charming Cherry Pop`,
  desc: `what other cherries you wanna pop? :flushed:`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702755`,
  name: `Sweet Baguette`,
  desc: `Nothing Beats the smell of fresh baguette`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702612`,
  name: `Fairy Pico`,
  desc: `Fairy Piccolo* OD`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702601`,
  name: `Bacon`,
  desc: `It's bacon.. how is this not tier 3?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702317`,
  name: `Bloody Ruby Sabre`,
  desc: `Trash.`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702266`,
  name: `Sunshine Pan`,
  desc: `you're the light to my tunnel`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702211`,
  name: `Blizzard Stick`,
  desc: `-1 vp for this weapon LOL`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702210`,
  name: `Santa Buddy`,
  desc: `S> 1 VP`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702479`,
  name: `バットソード`,
  desc: `I feel bad for ya`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702269`,
  name: `Mini Dawn Warrior`,
  desc: `Its like Dawn Warrior but mini`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702270`,
  name: `Mini Blaze Wizard`,
  desc: `There's a Blaze Wizard class?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702271`,
  name: `Mini Wind Archer`,
  desc: `She kinda hot`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702272`,
  name: `Mini Night Walker`,
  desc: `best cygnus knight class congratz!`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702273`,
  name: `Mini Thunder Breaker`,
  desc: `2nd best Cygnus knight Class`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702197`,
  name: `Tsunami Wave`,
  desc: `Florida gang where you at?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702198`,
  name: `Bullseye Board`,
  desc: `Steady your breath and aim.`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702200`,
  name: `Plastic umbrella`,
  desc: `What else are umbrella's made of?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702686`,
  name: `Sweet Pig Weapon`,
  desc: `you still the sweetest uwu`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702688`,
  name: `Superstar M`,
  desc: `Mango's Porn name`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702692`,
  name: `Chicken Cutie Weapon`,
  desc: `The Sky is Falling!`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702789`,
  name: `Pupmallow Pop Weapon`,
  desc: `TRASH`,
  type: `weapon`,
  rarity: `1`
},
{
itemID: `1702817`,
name: `Invincible Blade`,
desc: `A Blade that is in god mode pls ban`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1062214`,
name: `Teddy Hip Pants`,
desc: `The pants you wear to a rap battle`,
type: `bottom`,
rarity: `1`
},
{
itemID: `1702614`,
name: `Baseball Bat`,
desc: `Time to hit that homerun! `,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702672`,
name: `Duckling Cross Bag`,
desc: `you murderer!`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1073297`,
name: `Crispy Carrot Flippers`,
desc: `Tastes better than a crunchy carrot`,
type: `shoes`,
rarity: `1`
},
{
itemID: `1073303`,
name: `Starry Summer Night Shoes`,
desc: `Like a Nolstalgic summer days`,
type: `shoes`,
rarity: `1`
},
{
itemID: `1004720`,
name: `Umbral Cap`,
desc: `End Game Cap`,
type: `hat`,
rarity: `1`
},
{
itemID: `1073133`,
name: `Umbral Boots`,
desc: `I found this in my grandma's backyard!`,
type: `shoes`,
rarity: `1`
},
{
itemID: `1102912`,
name: `Umbral Cloak`,
desc: `refund booth is that way`,
type: `cape`,
rarity: `1`
},
{
itemID: `1012166`,
name: `Villain Mask`,
desc: `You are now a villian`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1012626`,
name: `Cadena Mask`,
desc: `My hands are bleeding send help - Brandon`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1012518`,
name: `Von Bon Mask`,
desc: `Von Bon did NOTHING to you!`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1012642`,
name: `Bubble Pup Mask`,
desc: `So Cute!`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1012636`,
name: `Dark Silence`,
desc: `it's an okay item`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1012501`,
name: `No Biting`,
desc: `Bad Boi`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1012511`,
name: `Cleaning Mask`,
desc: `Mask you wear while cleaning.`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1012610`,
name: `Engima`,
desc: `This Enigma is German`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1012450`,
name: `Choco Candy Cookie`,
desc: `Do you feel the sugar rush?`,
type: `faceAcc`,
rarity: `1`
},
{
itemID: `1702419`,
name: `Pink Bean Buddy`,
desc: `ewwww`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702436`,
name: `Galactic Legend`,
desc: `galactic sounds like a cool IGN`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702445`,
name: `Detective Glass`,
desc: `what do you deduce watson?`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702455`,
name: `RED Paint Bucket`,
desc: `Paint your house with this`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702443`,
name: `Puppeteer's Promise`,
desc: `this is og`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702450`,
name: `Tedimus Beartaculous`,
desc: `Used to Kill Great Egyptian leaders`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702446`,
name: `Sea Otter Slammer`,
desc: `TRASH`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702416`,
name: `Lord of the Carrots`,
desc: `Better show than LOTR`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702424`,
name: `Stylish Iron`,
desc: `A stylish iron indeed`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702381`,
name: `Twin Crescent Blade`,
desc: `The more the merrier`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702380`,
name: `Azure Crystal Crusher`,
desc: `TRASH`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702366`,
name: `Shark-sicle`,
desc: `Tastes really good!`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702340`,
name: `Rabbit in a Hat`,
desc: `and for my next trick!`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702372`,
name: `Pimp Chalice`,
desc: `real baller now!`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1702345`,
name: `Fierce Cat`,
desc: `Cat scratches`,
type: `weapon`,
rarity: `1`
},
{
itemID: `1022285`,
name: `Round Glasses`,
desc: `OwO`,
type: `eyeAcc`,
rarity: `1`
},
{
itemID: `1022279`,
name: `Black Eye Bandages`,
desc: `Who hurt you?`,
type: `eyeAcc`,
rarity: `1`
},
{
itemID: `1102622`,
name: `Princess of Time Pocket Watch`,
desc: `What time is it?`,
type: `cape`,
rarity: `1`
},
{
itemID: `1073309`,
name: `Cutie Pie High-Tops`,
desc: `Sorry, no refunds`,
type: `shoes`,
rarity: `1`
},
{
itemID: `1053351`,
name: `Cutie Pie Coat`,
desc: `QTPie`,
type: `top`,
rarity: `1`
},
{
itemID: `1042314`,
name: `Rabbit and Bear Shirt`,
desc: `Atleast its not a rabbit-bear`,
type: `top`,
rarity: `1`
},
{
  itemID: `1702687`,
  name: `Strawberry Bon Bon`,
  desc: `you ARE the strawberry on top uwu`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702682`,
  name: `Triple Fish Skewer`,
  desc: `What I order when I order take out`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702808`,
  name: `Iron Mace`,
  desc: `get a refund for ur VP`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702454`,
  name: `Seal Wave Snuggler`,
  desc: `Wave Back!`,
  type: `weapon`,
  rarity: `1`
},

{
  itemID: `1702208`,
  name: `Alligator Tube`,
  desc: `A worse Baby Ellie I'm sorry.`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702209`,
  name: `Rudolph Stick`,
  desc: `Is Christmas here already!?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702645`,
  name: `Phantom's Cane`,
  desc: `Who is Phantom?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702648`,
  name: `Maha the Polearm`,
  desc: `Maha the Best Job Leader don't @ me`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702650`,
  name: `Shining Rod of Equilibrium`,
  desc: `we all know Light Form is the best`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702651`,
  name: `Forgotten Hero's Knuckle`,
  desc: `gone but never forgotten.. oh wait`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702655`,
  name: `Lil Mercedes`,
  desc: `Trash Tier Class with God Tier Link Skill`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702656`,
  name: `Lil Luminous`,
  desc: `Are you Dark or Light gang?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702657`,
  name: `Lil Shade`,
  desc: `Forgotten Hero feels bad`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702665`,
  name: `Lil Evan`,
  desc: `My new rapper name`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702757`,
  name: `Cygnus's Guard`,
  desc: `You got a bodyguard now!`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702805`,
  name: `Party Scepter`,
  desc: `Budget Death Scythe`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702800`,
  name: `Strawberry Fitness Jump Rope`,
  desc: `eat your fruits!`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1102786`,
  name: `Titan Escape`,
  desc: `RUN YOU WEEBS`,
  type: `cape`,
  rarity: `1`
},
{
  itemID: `1702839`,
  name: `Bichan Bam`,
  desc: `???`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702840`,
  name: `Happy Ghost Weapon`,
  desc: `Happy Ghost Happy Life`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702877`,
  name: `Mu Young's Sword`,
  desc: `Take care of the sword well.`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702854`,
  name: `Maple Alliance Flag`,
  desc: `TRASH`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1005036`,
  name: `Ryude Hat`,
  desc: `The Sword is still better.`,
  type: `hat`,
  rarity: `1`
},
{
  itemID: `1005037`,
  name: `Crimson Fate Mark`,
  desc: `You are now marked, you will die in 3, 2, ....`,
  type: `hat`,
  rarity: `1`
},
{
  itemID: `1005083`,
  name: `Mist Puppy`,
  desc: `doggo on ur head`,
  type: `hat`,
  rarity: `1`
},
{
  itemID: `1005084`,
  name: `Flushed Puppy`,
  desc: `he's just a little shy`,
  type: `hat`,
  rarity: `1`
},
{
  itemID: `1004920`,
  name: `Green Tea Latte Hat`,
  desc: `A greet tea hat`,
  type: `hat`,
  rarity: `1`
},
{
  itemID: `1004921`,
  name: `Caramel Latte Hat`,
  desc: `Say Goodbye to your 1 VP`,
  type: `hat`,
  rarity: `1`
},
{
  itemID: `1004922`,
  name: `Strawberry Latte Hat`,
  desc: `so sweet just like you uwu`,
  type: `hat`,
  rarity: `1`
},  
{
  itemID: `1702842`,
  name: `Navy Telescope Weapon`,
  desc: `what do you see?`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1005237`,
  name: `Delinquent Bear Hat`,
  desc: `such a rebel`,
  type: `hat`,
  rarity: `1`
},
{
  itemID: `1702793`,
  name: `Eggplant of Doom`,
  desc: `:eggplant:`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1792623`,
  name: `Today Jay`,
  desc: `today is the day Jay`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702666`,
  name: `Lil Aran`,
  desc: `All these heros but black mage isn't dead yet`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702667`,
  name: `Lil Phantom`,
  desc: `Lost his girlfriend, can't relate`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702677`,
  name: `Lil Damien`,
  desc: `Any GMS fella will understand this boss`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702371`,
  name: `Pimp Stick`,
  desc: `You are now a god`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702557`,
  name: `Duster`,
  desc: `Stay Clean ;)`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702504`,
  name: `Frozen Heart`,
  desc: `Warning: Do not thaw!`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702473`,
  name: `Shadow Executor`,
  desc: `You are an edge lord now!`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702728`,
  name: `Sweet Jelly Paw`,
  desc: `Tastes like almonds`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702605`,
  name: `Donut`,
  desc: `tastes like chicken`,
  type: `weapon`,
  rarity: `2`
},
{
  itemID: `1102615`,
  name: `Clocktower Wind-up Doll`,
  desc: `Its haunted sleep well today :)`,
  type: `cape`,
  rarity: `1`
},
{
  itemID: `1702201`,
  name: `Bone Weapon`,
  desc: `Instantly grow wheat in minecraft`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702505`,
  name: `Breezy Bamboo`,
  desc: `Bamboo harvested from the dude's backyard in china`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702588`,
  name: `Black Cat Plush`,
  desc: `If a black Cat crosses your path its bad luck.`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702576`,
  name: `Ground Pounder`,
  desc: `Lookin like Hulk`,
  type: `weapon`,
  rarity: `1`
},
{
  itemID: `1702690`,
  name: `Noble Maple Rod`,
  desc: `Noble is top tier guild hehe`,
  type: `weapon`,
  rarity: `1`
},

  ],

  randomItemRare: [
    {
      itemID: `1102957`,
      name: `Chained Princess`,
      desc: `You're a chained soul, keep yourself locked up`,
      type: `cape`,
      rarity: `2`
    },
    {
    itemID: `1062233`,
    name: `Dark Slate Jeans`,
    desc: `Fresh from the jean factory!`,
    type: `bottom`,
    rarity: `2`
  }, 
  {
    itemID: `1032262`,
    name: `Umbral Earrings`,
    desc: `end game earrings`,
    type: `faceAcc`,
    rarity: `2`
  },
  {
    itemID: `1053022`,
    name: `Umbral Attire`,
    desc: `Lookin fresh!`,
    type: `overall`,
    rarity: `2`
  },
  {
    itemID: `Umbral Attire`,
    name: `Umbral Coat`,
    desc: `Its a bit cold out take this`,
    type: `overall`,
    rarity: `2`
  },
  {
    itemID: `1073132`,
    name: `Umbral Shoes`,
    desc: `Taiga is that way for your refund`,
    type: `shoes`,
    rarity: `2`
  },
  {
    itemID: `1012517`,
    name: `Vellum Mask`,
    desc: `Vellum did nothing to you!`,
    type: `faceAcc`,
    rarity: `2`
  },
  {
    itemID: `1012044`,
    name: `Mummy Mask`,
    desc: `Spooky!`,
    type: `faceAcc`,
    rarity: `2`
  },
  {
    itemID: `1115059`,
    name: `Plum Blossom Chat Ring`,
    desc: `just a ring`,
    type: `ring`,
    rarity: `2`
  },
  {
    itemID: `1115148`,
    name: `Plum Blossom Label Ring`,
    desc: `just a ring`,
    type: `ring`,
    rarity: `2`
  },
  {
    itemID: `1115054`,
    name: `Vampire Chat Ring`,
    desc: `just a ring`,
    type: `ring`,
    rarity: `2`
  },
  {
    itemID: `1115143`,
    name: `Vampire Label Ring`,
    desc: `just a ring`,
    type: `ring`,
    rarity: `2`
  },
  {
    itemID: `1112252`,
    name: `Red Rose Chat Ring`,
    desc: `just a ring`,
    type: `ring`,
    rarity: `2`
  },
  {
    itemID: `1112141`,
    name: `Red Rose Label Ring`,
    desc: `just a ring`,
    type: `ring`,
    rarity: `2`
  },
  {
    itemID: `1102766`,
    name: `Raging Lotus Aura`,
    desc: `She is mad oh lord.`,
    type: `cape`,
    rarity: `2`
  },
  {
    itemID: `1102748`,
    name: `Rabbit-Bear Camping Bag`,
    desc: `what the hell is a rabbit bear?`,
    type: `cape`,
    rarity: `2`
  },
  {
    itemID: `1073317`,
    name: `Little Star Cocoon Anklet`,
    desc: `looks really good on you`,
    type: `shoes`,
    rarity: `2`
  },
  {
    itemID: `1073218`,
    name: `Froggy Rainboots`,
    desc: `don't step in the puddles!`,
    type: `shoes`,
    rarity: `2`
  },
  {
    itemID: `1070059`,
    name: `Rainbow Picnic Shoes`,
    desc: `the Rainbow is our national color`,
    type: `shoes`,
    rarity: `2`
  },
  {
    itemID: `1051426`,
    name: `Gothic Girls Uniform`,
    desc: `A uniform for all the emo ladies`,
    type: `overall`,
    rarity: `2`
  },
  {
    itemID: `1070091`,
    name: `Crimson Fate Shoes`,
    desc: `LF> Party`,
    type: `shoes`,
    rarity: `2`
  },
  {
    itemID: `1072448`,
    name: `Rainbow Boots`,
    desc: `Does this make you a GAYmer?`,
    type: `shoes`,
    rarity: `2`
  },
    {
      itemID: `1792790`,
      name: `Golden Eventides`,
      desc: `Everything you touch now turns GOLD!`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702695`,
      name: `Overly Cute Puppy`,
      desc: `OD* Cute Puppy`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702689`,
      name: `Fairy Flora`,
      desc: `Tinker bells weapon she used in WW2`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702621`,
      name: `Mystery Dice`,
      desc: `No Game No Life?`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702565`,
      name: `Death's Scythe`,
      desc: `175$ in some servers!`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1702551`,
      name: `Korean Thanksgiving Persimmon Branch`,
      desc: `Korean Thanksgiving is like American Canaday day`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702523`,
      name: `Sunny Day Rainbow`,
      desc: `These colors remind you of Brandon`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702512`,
      name: `Crown Rod`,
      desc: `Pimp Cane`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702486`,
      name: `Fluttering Camellia Flower`,
      desc: `Wow a flower!`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702469`,
      name: `Arachne`,
      desc: ` ERROR ERROR msg.author`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702368`,
      name: `Iris Butterwand`,
      desc: `A blue Butterfly Weapon`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702367`,
      name: `Rose Butterwand`,
      desc: `A red Butterfly Weapon`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1102906`,
      name: `Snug Black Nero`,
      desc: `Goes well with the Nero Hat!`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1702795`,
      name: `Enchanting Flute`,
      desc: `https://www.youtube.com/watch?v=KolfEhV-KiA`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702814`,
      name: `Rainbow Dreamcloud Weapon`,
      desc: `I'm as straight as a rainbow!`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1004503`,
      name: `Cat Hood`,
      desc: `Now you're talking!`,
      type: `hat`,
      rarity: `2`
    },
    {
      itemID: `1003777`,
      name: `Goth Cat Hood`,
      desc: `Emo?`,
      type: `hat`,
      rarity: `2`
    },
    {
      itemID: `1042280`,
      name: `Min T-Shirt`,
      desc: `A shirt that lasts a min`,
      type: `top`,
      rarity: `2`
    },
    {
      itemID: `1042281`,
      name: `Jeong T-Shirt`,
      desc: `Jeong is my favorite singer!`,
      type: `top`,
      rarity: `2`
    },
    {
      itemID: `1042282`,
      name: `Eum T-Shirt`,
      desc: `Eum is two words combined`,
      type: `top`,
      rarity: `2`
    },
    {
      itemID: `1702202`,
      name: `Baby Ellie`,
      desc: `Cute little elephant!`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702678`,
      name: `Lil Alicia`,
      desc: `She drops ryude's sword`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1082728`,
      name: `Homeless Cat Gloves`,
      desc: `Gloves for that Homeless cat you passed by.`,
      type: `gloves`,
      rarity: `1`
    },
    {
      itemID: `1005111`,
      name: `Plum Blossom Bonnet`,
      desc: `Plum is a vegetable`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1005110`,
      name: `Plum Blossom Petals`,
      desc: `there are 100 billion Plums yet you are the sweetest one!`,
      type: `hat`,
      rarity: `1`
    },
    {
      itemID: `1053301`,
      name: `Homeless Cat Outfit`,
      desc: `why?`,
      type: `overall`,
      rarity: `2`
    },
    {
      itemID: `1702812`,
      name: `Lunar New Year VIP Weapon`,
      desc: `red envolopes?`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702811`,
      name: `Luminous Sea`,
      desc: `This doesn't make the Lumi class any better >:(`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702810`,
      name: `Soft Snow`,
      desc: `Soft Snow is the same as Hard Water`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1073270`,
      name: `Homeless Cat Shoes`,
      desc: `uwuwuwuwuwuwuwuwuwuwuwuwuwuwu`,
      type: `shoes`,
      rarity: `2`
    },
    {
      itemID: `1702816`,
      name: `Furry Bear`,
      desc: `what kinda bear isnt furry?`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702815`,
      name: `Plum Blossom`,
      desc: `Smells good.`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1103079`,
      name: `Plum Blossom Perfume Pouch`,
      desc: `walking down perfume aisle and you smell this`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1702868`,
      name: `Heavenly Prayer Weapon`,
      desc: `I'm still not religious`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702847`,
      name: `Cloudy Paper Plane Weapon`,
      desc: `Maybe you can win that paper airplane contest.`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702833`,
      name: `Oceanic Requiem Weapon`,
      desc: `Karthus is Pressing R!`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1702830`,
      name: `Spring Rain Parasol`,
      desc: `Spring is the season for Love <3`,
      type: `weapon`,
      rarity: `2`
    },
    {
      itemID: `1005139`,
      name: `Homeless Cat Hat`,
      desc: `Homeless cat kinda cute uwu`,
      type: `hat`,
      rarity: `2`
    },
    {
      itemID: `1103092`,
      name: `Homeless Cat Cape`,
      desc: `Homeless Cat cape for a Homeless weeb`,
      type: `cape`,
      rarity: `2`
    },
    
    {
      itemID: `1702863`,
      name: `Alliance Commander Wing Sword`,
      desc: `Brandon is OD`,
      type: `weapon`,
      rarity: `2`
    },
    
{
  itemID: `1702878`,
  name: `Regal Romance Parasol`,
  desc: `This still doesn't make you not single.`,
  type: `weapon`,
  rarity: `2`
},
    
{
  itemID: `1702892`,
  name: `Sugarsweet Candy Spear`,
  desc: `Christmas came early this year!`,
  type: `weapon`,
  rarity: `2`
},
    {
      itemID: `1082495`,
      name: `Cat Lolita Gloves`,
      desc: `Arm decorations belonging to an underage girl`,
      type: `gloves`,
      rarity: `2`
    },
{
  itemID: `1072468`,
  name: `Lolita Knee Socks Shoes`,
  desc: `Stockings belonging to an underage girl`,
  type: `shoes`,
  rarity: `2`
},
    {
      itemID: `1002976`,
      name: `Maid Headband`,
      desc: `Servant's headwear`,
      type: `hat`,
      rarity: `2`
    },
    
{
  itemID: `1051463`,
  name: `Bloody Bride`,
  desc: `The wedding dress of a maiden forced into confinement`,
  type: `overall`,
  rarity: `2`
},
    
{
  itemID: `1102157`,
  name: `Puppet Strings`,
  desc: `Sex slave's restraints`,
  type: `cape`,
  rarity: `2`
},
{
  itemID: `1103067`,
  name: `Bound By Dreams`,
  desc: `Stockholm Syndrome`,
  type: `cape`,
  rarity: `2`
},
{
  itemID: `1001113`,
  name: `Bloody Veil`,
  desc: `The wedding veil of a maiden forced into confinement`,
  type: `hat`,
  rarity: `2`
},
    {
      itemID: `1103126`,
      name: `Misty Nocturne`,
      desc: `Yall play league?`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1103127`,
      name: `Misty Fantasia`,
      desc: `Misty like the Gym Leader?`,
      type: `cape`,
      rarity: `2`
    }, 
    {
      itemID: `1103150`,
      name: `Regal Romance Cape`,
      desc: `I'm still lonely though..`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1102605`,
      name: `Shadow Peacemaker`,
      desc: `Edgy gang`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1102699`,
      name: `Magma Wings`,
      desc: `Super Hot`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1102811`,
      name: `Snow Bloom`,
      desc: `Snowwy uwu`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1012631`,
      name: `Bandaged Lips`,
      desc: `Keep your lips closed`,
      type: `faceAcc`,
      rarity: `2`
    },
    {
      itemID: `1102820`,
      name: `Hazy Night Tassel`,
      desc: `Night tasle for u`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1000009`,
      name: `Red M-Forcer Helmet`,
      desc: `Red Ranger is overrated`,
      type: `hat`,
      rarity: `2`
    },
    {
      itemID: `1012589`,
      name: `So Sleepy`,
      desc: `So Sleepy -_-`,
      type: `faceAcc`,
      rarity: `2`
    },
    {
      itemID: `1052838`,
      name: `Student Swimsuit`,
      desc: `Loli's swimwear`,
      type: `overall`,
      rarity: `2`
    },
    {
      itemID: `1004835`,
      name: `Chained Princess Ribbon`,
      desc: `Headwear of the defeated kingdom's princess`,
      type: `hat`,
      rarity: `2`
    },
    {
      itemID: `1102957`,
      name: `Chained Princess Chain`,
      desc: `Restraints of the defeated kingdom's princess`,
      type: `cape`,
      rarity: `2`
    },
    {
      itemID: `1053092`,
      name: `Chained Princess Coat`,
      desc: `Torn clothes of the defeated kingdom's princess`,
      type: `overall`,
      rarity: `2`
    },
    {
      itemID: `1022269`,
      name: `Chained Princess Face Accessory`,
      desc: `Blindfold of the defeated kingdom's princess`,
      type: `faceAcc`,
      rarity: `2`
    },
    {
      itemID: `1053092`,
      name: `Chained Princess Coat`,
      desc: `Torn clothes of the defeated kingdom's princess`,
      type: `overall`,
      rarity: `2`
    },
    {
      itemID: `1022269`,
      name: `Chained Princess Face Accessory`,
      desc: `Blindfold of the defeated kingdom's princess`,
      type: `faceAcc`,
      rarity: `2`
    },
  ],
  randomItemUltra: [
        {
      itemID: `1004589`,
      name: `Jay's Sterilized Kitty Eye Patch`,
      desc: `Okay now you're a pro.`,
      type: `hat`,
      rarity: `3`
    },
    {
    itemID: `1005001`,
    name: `Mellow Dino Head Wrap`,
    desc: `You will make a good looking dinosaur uwu`,
    type: `hat`,
    rarity: `3`
  },
  {
    itemID: `1005002`,
    name: `Sunny Dino Head Wrap`,
    desc: `Sunny was  my high school nickname!`,
    type: `hat`,
    rarity: `3`
  },
  {
    itemID: `1004384`,
    name: `Dinofrog Hat`,
    desc: `Frog plus Dino equals 2x Cuter`,
    type: `hat`,
    rarity: `3`
  },
  {
    itemID: `1051548`,
    name: `Snow Blossom Coat`,
    desc: `Any canadian bois out here?`,
    type: `overall`,
    rarity: `3`
  },
  {
    itemID: `1073258`,
    name: `Soft Snow Slippers`,
    desc: `Feels like soft snow hehe xd`,
    type: `shoes`,
    rarity: `3`
  },
  {
    itemID: `1012644`,
    name: `Teddy Surgeon Mask`,
    desc: `Wow a GOOD ITEM!`,
    type: `faceAcc`,
    rarity: `3`
  },
  {
    itemID: `1012645`,
    name: `Skeleton Surgeon Mask`,
    desc: `Wow a GOOD ITEM!`,
    type: `faceAcc`,
    rarity: `3`
  },
  {
    itemID: `1112135`,
    name: `Ink-and-Wash Painting Name Tag Ring`,
    desc: `Big Hene Hoe Item congratz!`,
    type: `ring`,
    rarity: `3`
  },
  {
    itemID: `1112238`,
    name: `Ink-and-Wash Thought Bubble Ring`,
    desc: `Wow i'm so jealous`,
    type: `ring`,
    rarity: `3`
  },
  {
    itemID: `1115035`,
    name: `Starry Night Orchid Chat Ring`,
    desc: `cool ring`,
    type: `ring`,
    rarity: `3`
  },
  {
    itemID: `1115124`,
    name: `Starry Night Orchid Label Ring`,
    desc: `Look at the Stars!`,
    type: `ring`,
    rarity: `3`
  },
  {
    itemID: `1115041`,
    name: `Camellia's Sword Chat Ring`,
    desc: `This Camellia girl has alot of stuff`,
    type: `ring`,
    rarity: `3`
  },
  {
    itemID: `1115130`,
    name: `Camellia's Sword Label Ring`,
    desc: `Camellia must have been rich`,
    type: `ring`,
    rarity: `3`
  },
  {
    itemID: `1032255`,
    name: `White Earphones`,
    desc: `Cut the Wires to be more of a flex`,
    type: `earrings`,
    rarity: `3`
  },
  {
    itemID: `1033000`,
    name: `Lucid's Earrings`,
    desc: `Veri cute!`,
    type: `earrings`,
    rarity: `3`
  },
  {
    itemID: `1103007`,
    name: `Demon Bag`,
    desc: `Look inside!`,
    type: `cape`,
    rarity: `3`
  },
  {
    itemID: `1073347`,
    name: `Blue Flame Hellion Shoes`,
    desc: `its on fire!`,
    type: `shoes`,
    rarity: `3`
  },
    
    {
      itemID: `1702756`,
      name: `Starlit Dreamweaver`,
      desc: `anyone ever read BFG?`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1702737`,
      name: `Frost Staff`,
      desc: `Staff like the mage weapon? if yk yk`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1702680`,
      name: `Camellia's Sword`,
      desc: `Sword cultivated from the fields of Camellia`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1702721`,
      name: `죽음의 키읔`,
      desc: `If only we knew korean`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1702718`,
      name: `Shadow Warrior's Sword`,
      desc: `Shadow Warrior is a cool guy for sure`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1004003`,
      name: `Pink Nero Hoodie`,
      desc: `Embrace the Furry fursona uwu`,
      type: `hat`,
      rarity: `3`
    },
    {
      itemID: `1004004`,
      name: `Grey Nero Hoodie`,
      desc: `emo Nero Hoodie`,
      type: `hat`,
      rarity: `3`
    },
    {
      itemID: `1003802`,
      name: `Green Dinosaur Hat`,
      desc: `Green Dino is so CUTE! uwu`,
      type: `hat`,
      rarity: `3`
    },
    {
      itemID: `1003803`,
      name: `Purple Dinosaur Hat`,
      desc: `Purple Dino Tier 1 in our hearts`,
      type: `hat`,
      rarity: `3`
    },
    {
      itemID: `1702875`,
      name: `Frostblade Weapon`,
      desc: `Roblox > Minecraft`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1702804`,
      name: `Take Me Too!`,
      desc: `Please daddy!`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1051551`,
      name: `Plum Blossom Dress`,
      desc: `easter egg message: Brandon is kinda gay.`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1702825`,
      name: `Homeless Cat Weapon`,
      desc: `lookin like rengar with this weapon.`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1702858`,
      name: `Seafoam Coral Blade`,
      desc: `A beautiful sword cultivated from the sea.`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1003730`,
      name: `Cat Lolita Hat`,
      desc: `Cute animal wear belonging to an underage girl`,
      type: `hat`,
      rarity: `3`
    },
    {
      itemID: `1052554`,
      name: `Cat Lolita Outfit`,
      desc: `Cozy dress belonging to an underage girl`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1052419`,
      name: `Pink Lolita Outfit`,
      desc: `High quality fabric belonging to an underage girl`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1051200`,
      name: `Bunny Girl`,
      desc: `Casino showgirl's tight-fitting garb`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1051174`,
      name: `Bikini`,
      desc: `Lewd swimwear`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1005046`,
      name: `Christmas Bunny Hat`,
      desc: `Casino showgirl's bunny ears`,
      type: `hat`,
      rarity: `3`
    },
    {
      itemID: `1032262`,
      name: `Umbral Earrings`,
      desc: `Big Hene hoe`,
      type: `earring`,
      rarity: `3`
    },
    {
      itemID: `1102824`,
      name: `Halfblood Wings`,
      desc: `We ain't crip out here`,
      type: `cape`,
      rarity: `3`
    },
    {
      itemID: `1042349`,
      name: `All About Black`,
      desc: `Biggest Hene Hoe shirt`,
      type: `top`,
      rarity: `3`
    },
    {
      itemID: `1051487`,
      name: `Cozy Fur Outfit`,
      desc: `Keep yourself Warm though Maple Winter`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1052671`,
      name: `Oversized Oxford`,
      desc: `Super Hene hoe Status`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1005174`,
      name: `Erda Hat`,
      desc: `Hat for the Erda Set`,
      type: `hat`,
      rarity: `3`
    },
    {
      itemID: `1103108`,
      name: `Erda Cape`,
      desc: `Cape for the Erda Set`,
      type: `cape`,
      rarity: `3`
    },
    {
      itemID: `1053318`,
      name: `Erda Outfit`,
      desc: `Outfit for the Erda Set`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1073283`,
      name: `Erda Shoes`,
      desc: `Shoes for the Erda Set`,
      type: `shoes`,
      rarity: `3`
    },
    {
      itemID: `1702841`,
      name: `Erda Weapon`,
      desc: `Weapon for the Erda Set`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1052671`,
      name: `Oversized Oxford`,
      desc: `Its okay you will grow into it.`,
      type: `overall`,
      rarity: `3`
    },
    {
      itemID: `1062112`,
      name: `Underpants`,
      desc: `if you have this item you OD`,
      type: `bottom`,
      rarity: `3`
    },
    {
      itemID: `1702585`,
      name: `Universal Transparent Weapon`,
      desc: `Best Weapon Don't @ Me`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1702792`,
      name: `Blood Oath Weapon`,
      desc: `Crip gang`,
      type: `weapon`,
      rarity: `3`
    },
    {
      itemID: `1012008`,
      name: `Censor`,
      desc: `for all you NTR watchers`,
      type: `faceAcc`,
      rarity: `3`
    },
    {
      itemID: `1022267`,
      name: `Personal Info Protection Stick`,
      desc: `Make your ahegao less obvious`,
      type: `eyeAcc`,
      rarity: `3`
    },
    {
      itemID: `1022259`,
      name: `Bandage Blindfold`,
      desc: `Wow you are a kinky person!`,
      type: `eyeAcc`,
      rarity: `3`
    },
  ],

};
