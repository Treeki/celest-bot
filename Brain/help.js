const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');




module.exports.doInfo = function(msg, discordID, randomThumb, args) {

  if (msg.channel.type === `dm`) {
    return;
  }
  let infocom = `info`;
  let infocom2 = `showinfo`;
  if (args[0] !== infocom.toLowerCase()) {
    if (args[0] !== infocom2.toLowerCase()) {
      return;
    }
  }

  function createBank() {
    sqlFunctions.getResult(`INSERT INTO bank (discordID, mesos) VALUES ('${discordID}', 0)`,function(err, results) {

    });
  }

sqlFunctions.getResult('SELECT * FROM accounts WHERE discordID = ' + discordID + '', function (err, results) {
  if (results[0]) {
    var rows = JSON.parse(JSON.stringify(results[0]));



    sqlFunctions.getResult(`SELECT * FROM bank WHERE discordID = ${discordID}`, function (err, results) {
      if (!results[0]) {
        createBank();
      }
      sqlFunctions.getResult(`SELECT * FROM bank WHERE discordID = ${discordID}`, function (err, results) {
        var rowsmeso = JSON.parse(JSON.stringify(results[0]));
        console.log(rowsmeso.mesos);

        if (!results[0]) {
          msg.channel.send(`You don't have an account!`);
          return;
        }

        function numberWithCommas(x) {
          console.log(`x`);
          if (x == `null`) {
            return;
          }
          return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        let realDate = `${msg.member.joinedAt}`;
        let getJoin = realDate.substring(4,16);

      const voteEmbed = new Discord.RichEmbed({
        title: `Hewwo ${msg.author.username}`,
        thumbnail: {
              url: randomThumb,
            },
        description: `I'll show you info about your account below`,
            fields: [
            {
              name: `Vote Points`,
              value: `${rows.vpoints}`,
              inline: true,
            },
            {
              name: `Donation Points`,
              value: `${rows.dpoints}`,
              inline: true,
            },
            {
              name: `Characters`,
              value: `0`,
              inline: true,
            },
            {
              name: `Banked Mesos`,
              value: `${numberWithCommas(rowsmeso.mesos)}`,
              inline: true,
            },
            {
              name: `Discord Messages`,
              value: `${numberWithCommas(rows.disMsg)}`,
              inline: true,
            },
            {
              name: `Celestial Cash`,
              value: `${numberWithCommas(rows.nxCredit)}`,
              inline: true,
            },
        ],
        footer: {
          text: `Joined Celestial At: ${getJoin}`,
        }
      });

    //msg.channel.send(`Check your DMs`)

    if (args[1]) {
      let playerLookUp = args[1].replace(/[&\/\\#,@+()$~%!.'":*?<>{}]/g, '');
      let playerLookUpLiteral = args[1];
    if (playerLookUp) {
      console.log(playerLookUp);
      if (isNaN(playerLookUp)) {
        msg.channel.send(`Make sure you tag someone that's in the server!`);
        return;
      }
      sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = '${playerLookUp}'`, function (err, results){
        var accountRow = JSON.parse(JSON.stringify(results[0]));
        sqlFunctions.getResult(`SELECT * FROM bank WHERE discordID = '${playerLookUp}'`, function (err, results){
          var bankRow = JSON.parse(JSON.stringify(results[0]));

          voteEmbed.title = `Let me get those stats for you...`;
          voteEmbed.description = `Stats for ${playerLookUpLiteral}`
          voteEmbed.fields[0] = {
            name: `Vote Points`,
            value: `${accountRow.vpoints}`,
            inline: true,
          };
          voteEmbed.fields[1] = {
            name: `Donation Points`,
            value: `${accountRow.dpoints}`,
            inline: true,
          };
          voteEmbed.fields[2] = {
            name: `Characters`,
            value: `0`,
            inline: true,
          };
          voteEmbed.fields[3] = {
            name: `Banked Mesos`,
            value: `${numberWithCommas(bankRow.mesos)}`,
            inline: true,
          };
          voteEmbed.fields[4] = {
            name: `Discord Messages`,
            value: `${numberWithCommas(accountRow.disMsg)}`,
            inline: true,
          };
          voteEmbed.fields[5] = {
            name: `Celestial Cash`,
            value: `${numberWithCommas(accountRow.nxCredit)}`,
            inline: true,
          };
          let otherisMarried = accountRow.disMarry;
          if (otherisMarried != 0) {
            voteEmbed.fields[6] = {
              name: ` 💒 Marriage`,
              value: `💙<@${accountRow.disMarry}> is married to ${playerLookUpLiteral}💙`,
            };
          }

          msg.channel.send(voteEmbed);
          return;

        });
      });
      return;
    }
  }
  let isMarried = rows.disMarry;

  console.log(rows.disMarry);
  if (isMarried != 0) {
    voteEmbed.fields[6] = {
      name: ` 💒 Marriage`,
      value: `💙<@${discordID}> is married to <@${rows.disMarry}>💙`,
    };
  }
        msg.channel.send(voteEmbed);
    });

    });

    return;
  } else {
    msg.channel.send(`You don't have an acount silly, I'll dm you and help you get one set up uwu`);
    msg.author.send("hai it looks like you wanted to make an account, just say 'register' and we can go from there uwu");
    return;
  }
});
}

module.exports.doHelp = function(msg, discordID, randomThumb, args) {

  sqlFunctions.getResult(`SELECT * FROM accounts WHERE discordID = ${discordID}`, function(err, results) {
    if (results[0]) {
      let helpEmbed = new Discord.RichEmbed({
        title: `<a:Celest:610021151820873738> Celestial Help Section <a:Celest:610021151820873738>`,
        thumbnail: randomThumb,
        fields: [
          {
            name: `🔒Account Commands🔒`,
            value: `These are commands you may use now that you have an account with CelestialMS!`,
          },
          {
            name: `Account`,
            value: `Using this command will show you your game account info.\rExample: Celest account`, 
            inline: true, 
          },
          {
            name: `Vote`,
            value: `Example: Celest vote\rYou can vote once every 12 hours for 2 VP!`, 
            inline: true, 
          },
          {
            name: `Donate`,
            value: `Love the server so much you want to donate?\rType "donate" In my DMs!`, 
            inline: true, 
          },
          {
            name: `Give`,
            value: `This command will let you give Mesos/Vote Points/Donor Points To anyone in the discord!\rExample: Celest give @taiga 1000 vp`, 
            inline: true, 
          },
          {
            name: `Character`,
            value: `(You may only use this command if You have a character in game)\rThis will display your main character and their stats!`, 
            inline: true, 
          },
          {
            name: `Edit`,
            value: `This will allow you to edit your account information!\rExample: Edit username Taiga`, 
            inline: true, 
          },
          {
            name: `Unstuck`,
            value: `(You may only use this command if You have a character in game)\rGetting an error trying to login? Try this command out!`, 
            inline: true, 
          },
          {
            name: `🖍Fun Commands🖍`,
            value: `Commands You'll use to have some fun in the discord!`,
          },
          {
            name: `Flip`,
            value: `(This command will only work in the #Gambel Channel in CelestialMS)\rGamble your mesos / vote points / Donor Points with "Celest flip 100 mesos"!`, 
            inline: true, 
          },
          {
            name: `Gacha`,
            value: `(You may only use this command if You have a character in game)\rYou can use vote points to get random rewards for in game such as\r /Chairs/Cosmetics/NX/Scrolls/`, 
            inline: true, 
          },
          {
            name: `Rankings`,
            value: `Check The Ranking board for Celest!\rYou can see rankings for Mesos/Messages/Vote Points/Donor Points`, 
            inline: true, 
          },
          {
            name: `Marry`,
            value: `Want to marry someone on the discord? Type "Marry @Taiga"!`, 
            inline: true, 
          },
          {
            name: `Fame`,
            value: `(You may only use this command if You have a character in game)\rFame another character in CelestialMS!`, 
            inline: true, 
          },
        ]
      });
      msg.author.send(helpEmbed);
    } else {
      let helpEmbed = new Discord.RichEmbed({
        title: `<a:Celest:610021151820873738> Celestial Help Section <a:Celest:610021151820873738>`,
        thumbnail: randomThumb,
        fields: [
          {
            name: `📜Basic Commands📜`,
            value: `Basic commands you may use without an account!\r\nUse the commands how they're shown in the example!`,  
          },
          {
            name: `Register`,
            value: `Use this command to create an account with CelestialMS!\r\nExample: "register"`, 
            inline: true, 
          },
          {
            name: `Rankings`,
            value: `Check The Ranking board for Celest!\r\nExample: "celest rankings"`, 
            inline: true, 
          },
          {
            name: `Link`,
            value: `Signed up on the website? Use this to link your account to discord!\r\nExample: "celest link taiga"`, 
            inline: true, 
          },
          {
            name: `To see all commands, you must be a registered member!`,
            value: `After you register, you will earn rewards for talking and interacting with the discord that you can use in game!`,  
          }
          
        ]
      });
      msg.author.send(helpEmbed);
    }
  });

msg.channel.send(`Check your DMs ${msg.author.username}, i sent you some info to help you out 🔥`);
return;

}
