const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');
var jobs = require('../Brain/jobs');
const https = require('https');



module.exports.doEvent = function(msg, discordID, randomThumb, args) {

    if (msg.content.toLowerCase().includes(`start event`)) {

        if (!msg.member.hasPermission("ADMINISTRATOR")){
            return;
        }
            

        const filter = (reaction, user) => reaction.emoji.name === "👍";

        let collectedAmount = new Array;

        let collector = msg.createReactionCollector(filter, { time: 500000000 });
        collector.on('collect', (reaction, collector) => {

            collectedAmount.push(reaction);

            if (reaction.count == `8`) {
                collector.stop();
            }

            msg.channel.send(`${reaction.count - 1} out of 10 spots were filled!`);
            console.log(reaction.count);
        });
        collector.on('end', collected => {
            console.log(`collected ${collected.size} reactions`);
            msg.channel.send(`No More Spots!`);
        });

                msg.react('👍');

    }

}

module.exports.application = function(msg, discordID, randomThumb, args, celest) {

    if (msg.content.toLowerCase().includes(`apply`)) {

        if (msg.channel.type !== 'dm') {
            msg.channel.send(`Please apply in my Direct Messages!`);
            return;
        }


        let appFeilds = new Array;
        const embed = new Discord.RichEmbed({
            title: 'Celestial Staff Application!',
            thumbnail: {
                  url: randomThumb,
                },
            description: 'Fill out this form to submit your application!',
                fields: appFeilds,
                footer: {
                    text: `Type Cancle at anytime to end the application Process!`,
                }
          });

          let appFeild0 = Object.assign({}, embed.fields[0]);
          let appFeild1 = Object.assign({}, embed.fields[1]);

                appFeild0.name = `How old are you?`;
                appFeild0.value = `Example: 18 years old`;

                appFeilds.push(appFeild0);
    
    
        var msgId = new Array;
    
        msg.channel.send(embed).then(sent => {
          msgId.push(sent.id);
        });
    
        var messages = new Array;
    
    
        const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 9000000 });
        collector.on('collect', msg => {

            if (msg.content.toLowerCase().includes(`Cancle`)) {
                appFeild0.name = `Application Cancled`;
                appFeild0.value = `Try applying again!`;
                collector.stop();
                return;
            }

    
          //var messageArray = Array.from(collection.values());
            sqlFunctions.getResult('SELECT * FROM accounts WHERE discordID = ' + discordID + '', function (error, results, fields) {
              if(results[0]) {
                var rows = JSON.parse(JSON.stringify(results[0]));
                //return msg.channel.send(`you already have an account, your username is ${rows.name} silly`);
    
                messages.push(msg.content);

               
    
    
    
        
    
    
    
                  if (messages[0]) { // username logic
    
                    appFeild0.name = `What's your time zone?`;
                    appFeild0.value = `Example: -5:00 GMT`;
    
    
    
                    if (messages[1]) { //pasword logic
    
                        appFeild0.name = `What's your Desiered Position?`;
                        appFeild0.value = `Example: GFX Artist / Game Master / Discord Modorator`;
                    }
    
                    if (messages[2]) { //email logic
    
                        appFeild0.name = `Can you write me a short paragraph on how you think you'd beable to help CelestialMS?`;
                        appFeild0.value = `Example: Hi My name is ---`;
    
    
                    }
                    if (messages[3]) { //birthday logic
                        appFeild0.name = `If 2 people were fighting in discord, what woul dyou do to diffuse the situation?`;
                        appFeild0.value = `Example: I would do ---`;
    
                    }
                    if (messages[4]) { //confirm logic
    
                        appFeild0.name = `What is your favorite anime?`;
                        appFeild0.value = `Example: Sword art online`;
                    }

                    if (messages[5]) { //confirm logic
    
                        appFeild0.name = `Do you have any previous experience with Modoration of any kind? If yes explain.`;
                        appFeild0.value = `Example: Yes, I did ---`;
                    }
                    if (messages[6]) { //confirm logic
    
                        appFeild0.name = `What do you like most about CelestialMS?`;
                        appFeild0.value = `Example: Taiga`;
                    }
                    if (messages[7]) { //confirm logic
    
                        appFeild0.name = `Do you like Celest (Me the bot)`;
                        appFeild0.value = `Example: Yes.`;
                    }
                    if (messages[8]) { //confirm logic
    
                        appFeild0.name = `What is your favorite emoji?`;
                        appFeild0.value = `Example: 🃏`;
                    }
                    if (messages[9]) { //confirm logic
    
                        appFeild0.name = `If there was one thing you could add or change about the server, what would it be?`;
                        appFeild0.value = `Example: Something.`;
                    }

                    if (messages[10]) { //confirm logic
    
                        appFeild0.name = `Final Thoughts?`;
                        appFeild0.value = `Example: Something.`;
                    }

                    if (messages[11]) { //confirm logic
    
                        appFeild0.name = `Okay thank you!`;
                        appFeild0.value = `Taiga and Sporky will review your application!`;
                        celest.channels.get('601998408093401108').send(
                            `🃏Application for ${msg.author.username}🃏\r|| Age: ${messages[0]}\rTime Zone: ${messages[1]}\rPosition: ${messages[2]}\r\n\r\nParagraph:\r\n ${messages[3]}\r2 People Fighting:\r\n ${messages[4]}\r ||`
                             );
                             celest.channels.get('601998408093401108').send(
                                `|| Favorite Anime: ${messages[5]}\rPrevious Experience:\r\n ${messages[6]}\rMost Liked:\r\n ${messages[7]}\rLike Celest?:\r\n ${messages[8]}\rFavorite Emoji: ${messages[9]}\rSomething to change:\r\n ${messages[10]}\rFinal Thoughts:\r\n ${messages[11]}\r ||`
                                 );
                         console.log(messages);
                         collector.stop();
                        
                    }
    
                  }
    
    
    
    
                  // update 'field' with new value
    
                  // create new embed with old title & description, new field
                  const newEmbed = new Discord.RichEmbed({
                    title: embed.title,
                    thumbnail: embed.thumbnail,
                    //description: `Answer my questons below and we'll get you started!`,
                    fields: appFeilds,
                    footer: {
                        text: `Please wait for my text box to change before typing anything else!`,
                    }
                  });
    
                  console.log(msg.channel.fetchMessages({around: msgId, limit: 1}))
    
                  msg.channel.fetchMessages({around: msgId, limit: 1})
                    .then(msg => {
                        const fetchedMsg = msg.first();
                        fetchedMsg.edit(newEmbed);
                    });
                  // edit message with new embed
                  // NOTE: can only edit messages you author
                  //msg.edit(newEmbed)
                    //.then(newMsg => console.log(`new embed added`))
                    //.catch(console.log);
    
    
    
              }
            });
      });
    
      collector.on('end', collected => {
        //Write actual code to send message to use if it timed out;
        console.log(`Finished Making account`);
      });
    }

    }

    module.exports.tester = function(msg, discordID, randomThumb, args, celest) {

        if (msg.content.toLowerCase().includes(`tester`)) {
            if (msg.channel.type !== 'dm' && (!msg.member.roles.has(`621605687570726922`) || !msg.member.roles.has(`621606140366553098`) || !msg.member.roles.has(`621605922846015489`))) {
                msg.channel.send(`Tester applications are only open to Celestial Founders!`);
                return;
            }
    
            if (msg.channel.type !== 'dm') {
                msg.channel.send(`Please apply in my Direct Messages!`);
                return;
            }
    
    
            let appFeilds = new Array;
            const embed = new Discord.RichEmbed({
                title: 'Celestial v208 Tester Application',
                thumbnail: {
                      url: randomThumb,
                    },
                description: 'Fill out this form to submit your application!',
                    fields: appFeilds,
                    footer: {
                        text: `Type Cancle at anytime to end the application Process!`,
                    }
              });
    
              let appFeild0 = Object.assign({}, embed.fields[0]);
              let appFeild1 = Object.assign({}, embed.fields[1]);
    
                    appFeild0.name = `How long have you been playing maplestory?`;
                    appFeild0.value = `Example: 10 Years`;
    
                    appFeilds.push(appFeild0);
        
        
            var msgId = new Array;
        
            msg.channel.send(embed).then(sent => {
              msgId.push(sent.id);
            });
        
            var messages = new Array;
        
        
            const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 9000000 });
            collector.on('collect', msg => {
    
                if (msg.content.toLowerCase().includes(`cancle`)) {
                    appFeild0.name = `Application Cancled`;
                    appFeild0.value = `Try applying again!`;
                    collector.stop();
                    return;
                }
    
        
              //var messageArray = Array.from(collection.values());
                sqlFunctions.getResult('SELECT * FROM accounts WHERE discordID = ' + discordID + '', function (error, results, fields) {
                  if(results[0]) {

                    var rows = JSON.parse(JSON.stringify(results[0]));

                    let founder = rows.founder;

                    if (founder == 0) {
                        msg.channel.send(`You must be a founder to apply to be a tester!`);
                        collector.stop();
                        return;
                    }
                    //return msg.channel.send(`you already have an account, your username is ${rows.name} silly`);
        
                    messages.push(msg.content);

                    const finish = new Discord.RichEmbed({
                        title: `${msg.author.username} Tester Application`,
                        thumbnail: {
                              url: randomThumb,
                            },
                        description: `Here's the results:`,
                      });

                      if (messages[0]) { 
        
                        appFeild0.name = `Are you familiar with uptodate content`;
                        appFeild0.value = `Example: I have Defeated the black mage on GMS, etc etc`;
                        finish.fields[0] = {
                            name: `Q: Time Playing Maplestory`,
                            value: `A: ${messages[0]}`,
                        }
        
        
        
                        if (messages[1]) { 
        
                            appFeild0.name = `What is the highest total stat you have achieved playing maplestory?`;
                            appFeild0.value = `Example: 30k INT on a Battle Mage`;
                            finish.fields[1] = {
                                name: `Q: Familiar with Up to Date Content`,
                                value: `A: ${messages[1]}`,
                            }
                        }
        
                        if (messages[2]) { 
        
                            appFeild0.name = `What classes are you familiarized with?`;
                            appFeild0.value = `Example: I've mained Zero for 6 years, I know their skills in and out such as.. etc etc`;
                            finish.fields[2] = {
                                name: `Q: Highest Stat`,
                                value: `A: ${messages[2]}`,
                            }
        
        
                        }
                        if (messages[3]) { 
                            appFeild0.name = `What is the hardest boss you have fought in maplestory?`;
                            appFeild0.value = `Example: Black Mage etc etc`;
                            finish.fields[3] = {
                                name: `Q: Familiar Class`,
                                value: `A: ${messages[3]}`,
                            }
        
                        }
                        if (messages[4]) { 
        
                            appFeild0.name = `Are you familiar with maplestory bossing / general mechanics?`;
                            appFeild0.value = `Example: Yes, for example I know how all of the mechanics for Will work during his 2nd phase--`;
                            finish.fields[4] = {
                                name: `Q: Hardest Boss`,
                                value: `A: ${messages[4]}`,
                            }
                        }
    
                        if (messages[5]) { 
        
                            appFeild0.name = `What is the Highest level you've achived on GMS and what Class is that character?`;
                            appFeild0.value = `Example: 275 F/P Mage`;
                            finish.fields[5] = {
                                name: `Q: Mechanic Knowledge`,
                                value: `A: ${messages[5]}`,
                            }
                        }

                        if (messages[6]) { 
                            if (messages[6].includes(`2`)) {
                                appFeild0.name = `I see you answered ${messages[6]},  Being such a high level, Would you be willing to participate in SNiffing from GMS?`;
                                appFeild0.value = `Example: Yes / No`;
                                finish.fields[6] = {
                                    name: `Q: Highest Level Achieved on GMS`,
                                    value: `A: ${messages[6]}`,
                                }

                            } else {
                                appFeild0.name = `Thank you for Submitting your application!`;
                                appFeild0.value = `Your application will be reviewed by Taiga, Sporky, and the Devlopers!`;
                                finish.fields[6] = {
                                    name: `Q: Highest Level Achieved on GMS`,
                                    value: `A: ${messages[6]}`,
                                }

                            celest.channels.get('632262065246961665').send(finish);
                             console.log(messages);
                             collector.stop();

                            }
    
                        }

                        if (messages[7]) {
                            if (messages[7].toLowerCase().includes(`yes`)) {
                                appFeild0.name = `Okay great! You will be given a program to use if you are selected and you will be taught how to use it.`;
                                appFeild0.value = `Thank you so much for the application.`;
                                finish.fields[7] = {
                                    name: `Q: Player is above level 200: Willing to sniff`,
                                    value: `A: ${messages[7]}`,
                                }
                                celest.channels.get('632262065246961665').send(finish);
                                 console.log(messages);
                                 collector.stop();

                            } else {
                                appFeild0.name = `Thank you for Submitting your application!`;
                                appFeild0.value = `Your application will be reviewed by Taiga, Sporky, and the Devlopers!`;
                                finish.fields[7] = {
                                    name: `Q: Player is above level 200: Willing to sniff`,
                                    value: `A: ${messages[7]}`,
                                }
                                celest.channels.get('632262065246961665').send(finish);
                             console.log(messages);
                             collector.stop();

                            }  
                        }

        
                      }
        
        
        
                      const newEmbed = new Discord.RichEmbed({
                        title: embed.title,
                        thumbnail: embed.thumbnail,
                        
                        fields: appFeilds,
                        footer: {
                            text: `Please wait for my text box to change before typing anything else!`,
                        }
                      });
        
                      console.log(msg.channel.fetchMessages({around: msgId, limit: 1}))
        
                      msg.channel.fetchMessages({around: msgId, limit: 1})
                        .then(msg => {
                            const fetchedMsg = msg.first();
                            fetchedMsg.edit(newEmbed);
                        });
                      // edit message with new embed
                      // NOTE: can only edit messages you author
                      //msg.edit(newEmbed)
                        //.then(newMsg => console.log(`new embed added`))
                        //.catch(console.log);
        
        
        
                  }
                });
          });
        
          collector.on('end', collected => {
            //Write actual code to send message to use if it timed out;
            console.log(`Finished Making account`);
          });
        }
    
        }

        module.exports.report208 = function(msg, discordID, randomThumb, args, celest) {

            if (msg.content.toLowerCase().includes(`report`)) {
                if (msg.channel.type !== 'dm' && (!msg.member.roles.has(`621605687570726922`) || !msg.member.roles.has(`621606140366553098`) || !msg.member.roles.has(`621605922846015489`))) {
                    msg.channel.send(`You must be a v208 Tester to Report a Bug`);
                    return;
                }
        
                if (msg.channel.type !== 'dm') {
                    msg.channel.send(`Please Report in my Direct Messages!`);
                    return;
                }
        
        
                let appFeilds = new Array;
                const embed = new Discord.RichEmbed({
                    title: 'Celestial v208 Bug Report',
                    thumbnail: {
                          url: `https://static.thenounproject.com/png/83830-200.png`,
                        },
                    description: 'Lets Squash these bugs for a swift v208 Release~',
                        fields: appFeilds,
                        footer: {
                            text: `Type Cancle at anytime to end the application Process!`,
                        }
                  });
        
                  let appFeild0 = Object.assign({}, embed.fields[0]);
                  let appFeild1 = Object.assign({}, embed.fields[1]);
        
                        appFeild0.name = `What kind of bug is it?`;
                        appFeild0.value = `Example: NPC / Skill / Item / Etc`;
        
                        appFeilds.push(appFeild0);
            
            
                var msgId = new Array;
            
                msg.channel.send(embed).then(sent => {
                  msgId.push(sent.id);
                });
            
                var messages = new Array;
            
            
                const collector = new Discord.MessageCollector(msg.channel, m => m.author.id === msg.author.id, { time: 9000000 });
                collector.on('collect', msg => {
        
                    if (msg.content.toLowerCase().includes(`cancle`)) {
                        appFeild0.name = `Application Cancled`;
                        appFeild0.value = `Try applying again!`;
                        collector.stop();
                        return;
                    }

                    // tester channel 612227273520578562
        
            
                  //var messageArray = Array.from(collection.values());
                    sqlFunctions.getResult('SELECT * FROM accounts WHERE discordID = ' + discordID + '', function (error, results, fields) {
                      if(results[0]) {
    
                        var rows = JSON.parse(JSON.stringify(results[0]));
    
                        let founder = rows.founder;
    
                        if (founder == 0) {
                            msg.channel.send(`You must be a v208 Tester to report bugs!`);
                            collector.stop();
                            return;
                        }
                        //return msg.channel.send(`you already have an account, your username is ${rows.name} silly`);
            
                        messages.push(msg.content);

                        let bugType;
    
                        const finish = new Discord.RichEmbed({
                            title: `${msg.author.username}'s Bug Report`,
                            thumbnail: {
                                url: `https://static.thenounproject.com/png/83830-200.png`,
                                },
                            description: `This is what they Reported:`,
                          });
    
                          if (messages[0]) { 
                             bugType = messages[0].toLowerCase();

                            switch (bugType) {
                                case `npc`:
                                        appFeild0.name = `What is the ID and Name of the NPC?`;
                                        appFeild0.value = `Example: 9900000 NimaKin`;
                                        finish.fields[0] = {
                                            name: `Bug Type`,
                                            value: `${bugType}`,
                                        }
                                break;
                                case `skill`:
                                        appFeild0.name = `What is the ID and Name of the Skill?`;
                                        appFeild0.value = `Example: Maple Warrior 5221000`;
                                        finish.fields[0] = {
                                            name: `Bug Type`,
                                            value: `${bugType}`,
                                        }
                                break;
                                case `item`:
                                        appFeild0.name = `What is the ID of the Item?`;
                                        appFeild0.value = `Example: 4310096 Boss Coin`;
                                        finish.fields[0] = {
                                            name: `Bug Type`,
                                            value: `${bugType}`,
                                        }
                                break;
                                case `etc`:
                                        appFeild0.name = `Is there a script ID Involved?`;
                                        appFeild0.value = `Example: Enter_9999999.py`;
                                        finish.fields[0] = {
                                            name: `Bug Type`,
                                            value: `${bugType}`,
                                        }
                                break;
                                default: 
                                    appFeild0.name = `That's not a supported bug type!`;
                                    appFeild0.value = `Try typing " NPC / Skill / Item / ETC" Or suggest this bug type to taiga!`;
                                    messages.pop(messages[0]);
                                break;

                            }
                        }
            
            
            
            
                            if (messages[1]) { 
            
                                switch (bugType) {
                                    case `npc`:
                                            appFeild0.name = `What is the NPC doing wrong.`;
                                            appFeild0.value = `Example: NPC is supposed to open a shop and sell xxx`;
                                            finish.fields[1] = {
                                                name: `ID Of Npc`,
                                                value: `${messages[1]}`,
                                            }
                                    break;
                                    case `skill`:
                                            appFeild0.name = `What about the skill is wrong?`;
                                            appFeild0.value = `Example: There is no Cooldown, The cooldown should be 120 seconds`;
                                            finish.fields[1] = {
                                                name: `ID Of Skill`,
                                                value: `${messages[1]}`,
                                            }
                                    break;
                                    case `item`:
                                            appFeild0.name = `What's wrong with the item?`;
                                            appFeild0.value = `Example: Use item isn't giving correct items/Not taking away from inventory`;
                                            finish.fields[1] = {
                                                name: `ID Of Item`,
                                                value: `${messages[1]}`,
                                            }
                                    break;
                                    case `etc`:
                                            appFeild0.name = `What is wrong with it?`;
                                            appFeild0.value = `Example: Map portal doesn't work (Portal ID: enter_999999.py)`;
                                            finish.fields[1] = {
                                                name: `Script ID`,
                                                value: `${messages[1]}`,
                                            }
                                    break;
                                }
                            }
                            
            
                            if (messages[2]) {
                                switch (bugType) {
                                    case `npc`:
                                            appFeild0.name = `Thank you for reporting this NPC!`;
                                            appFeild0.value = `You can see your report in the Report Channel in Celest.`;
                                            finish.fields[2] = {
                                                name: `Problem with NPC`,
                                                value: `${messages[2]}`,
                                            }
                                    break;
                                    case `skill`:
                                            appFeild0.name = `Thank you for reporting this Skill!`;
                                            appFeild0.value = `You can see your report in the Report Channel in Celest.`;
                                            finish.fields[2] = {
                                                name: `Problem with Skill`,
                                                value: `${messages[2]}`,
                                            }
                                    break;
                                    case `item`:
                                            appFeild0.name = `Thank you for reporting this Item!`;
                                            appFeild0.value = `You can see your report in the Report Channel in Celest.`;
                                            finish.fields[2] = {
                                                name: `Problem With Item`,
                                                value: `${messages[2]}`,
                                            }
                                    break;
                                    case `etc`:
                                            appFeild0.name = `Thank you for reporting this Item!`;
                                            appFeild0.value = `You can see your report in the Report Channel in Celest.`;
                                            finish.fields[2] = {
                                                name: `Etc Problem:`,
                                                value: `${messages[2]}`,
                                            }
                                    break;
                                }
                            celest.channels.get('612227273520578562').send(finish);
                             console.log(messages);
                             collector.stop(); 
                            }

    
            
                          
            
            
            
                          const newEmbed = new Discord.RichEmbed({
                            title: embed.title,
                            thumbnail: embed.thumbnail,
                            
                            fields: appFeilds,
                            footer: {
                                text: `Please wait for my text box to change before typing anything else!`,
                            }
                          });
            
                          console.log(msg.channel.fetchMessages({around: msgId, limit: 1}))
            
                          msg.channel.fetchMessages({around: msgId, limit: 1})
                            .then(msg => {
                                const fetchedMsg = msg.first();
                                fetchedMsg.edit(newEmbed);
                            });
                          // sigh
            
            
            
                      }
                      
                    });
              });
            
              collector.on('end', collected => {
                //Write actual code to send message to use if it timed out;
                console.log(`Finished Making account`);
              });
            }
        
            }
    