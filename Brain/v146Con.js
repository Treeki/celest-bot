const mysql = require('mysql');
const settings = require(`./settings`);

var sql = mysql.createPool({
  host     : settings.v146sqlHost,
  user     : settings.v146sqlUsername,
  password : settings.v146sqlPassword,
  database : settings.v146sqlDatabase
});

function executeQuery(query, callback) {
  sql.getConnection(function (err, connection) {
    if (err) {
        return callback(err, null);
    }
    else if (connection) {
        connection.query(query, function (err, rows, fields) {
            connection.release();
            if (err) {
                return callback(err, null);
            }
            return callback(null, rows);
        })
    }
    else {
        return callback(true, "No Connection");
    }
  });
}


module.exports.getResult = function (query,callback) {
  executeQuery(query, function (err, rows) {
     if (!err) {
        callback(null,rows);
     }
     else {
        callback(true,err);
     }
  });
}


