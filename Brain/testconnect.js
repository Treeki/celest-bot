const mysql = require('mysql');
const settings = require(`./settings`);

var sql = mysql.createPool({
  host     : settings.v207sqlHost,
  user     : settings.v207sqlUsername,
  password : settings.v207sqlPassword,
  database : `v208`
});

function executeQuery(query, callback) {
  sql.getConnection(function (err, connection) {
    if (err) {
        return callback(err, null);
    }
    else if (connection) {
        connection.query(query, function (err, rows, fields) {
            connection.release();
            if (err) {
                return callback(err, null);
            }
            return callback(null, rows);
        })
    }
    else {
        return callback(true, "No Connection");
    }
  });
}


module.exports.sql = function (query,callback) {
  executeQuery(query, function (err, rows) {
     if (!err) {
        callback(null,rows);
     }
     else {
        callback(true,err);
     }
  });
}


