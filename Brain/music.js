const ytdl = require('ytdl-core');
const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');
var settings = require('../Brain/settings');





module.exports.getMusic = function(msg, discordID, randomThumb, args, celest, prefix) {


    if (msg.channel.id === `610984831337365531`) { // This will only let these commands to work in #music
      const message = msg.content.trim();
      const command = message.substring(prefix.length).split(/[ \n]/)[0].trim();
      const suffix = message.substring(prefix.length + command.length).trim();
        //suffix.trim(args[2]);
       // console.log(suffix);

       if (msg.content.toLowerCase().includes(`music`)) {
          let help = new Discord.RichEmbed({
            title: `Music Help Commands`,
            fields: [
              {
                name: `Play`,
                value: `Example: "Celest play Any song name"\rThen she wil play the song you searched. `,
                inline: true
              },
              {
                name: `Skip`,
                value: `Example: Celest Skip\rWill skip the current song`,
                inline: true
              },
              {
                name: `Pause`,
                value: `Example: Celest Pause\rWil Pause the current song`,
                inline: true
              },
              {
                name: `Resume`,
                value: `Example: Celest resume\rWill resume the song if it's paused`,
                inline: true
              }
            ],
            footer: {
              text: `Misusing my music talents will result in ur DJ role being revoked uwu`,
            }
          });
          msg.channel.send(help);
       }
        

        if (msg.content.toLowerCase().includes(`play`)) {
          celest.music.bot.playFunction(msg, suffix);
          return;
        }

        if (msg.content.toLowerCase().includes(`skip`)) {
          console.log(`Skipped Song.`);
        celest.music.bot.skipFunction(msg, suffix);
        return;
      }

      if (msg.content.toLowerCase().includes(`pause`)) {
        console.log(`Paused Song.`);
      celest.music.bot.pauseFunction(msg, suffix);
      return;
    }

    if (msg.content.toLowerCase().includes(`resume`)) {
      console.log(`Resumed Song.`);
    celest.music.bot.resumeFunction(msg, suffix);
    return;
  }
          
    }
 
}