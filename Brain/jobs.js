module.exports.getJobs = [
    {
        "id": 0,
        "name": "Beginner"
    },
    {
        "id": 100,
        "name": "Swordsman (1st)",
    },
    {
      "id": 1000,
      "name": "Noblesse"
    },
    {
      "id": 110,
      "name": "Fighter (2nd)"
    },
    {
      "id": 1100,
      "name": "Dawn Warrior (1st)"
    },
    {
      "id": 111,
      "name": "Crusader (3rd)"
    },
    {
      "id": 1110,
      "name": "Dawn Warrior (2nd)"
    },
    {
      "id": 1111,
      "name": "Dawn Warrior (3rd)"
    },
    {
      "id": 1112,
      "name": "Dawn Warrior (4th)"
    },
    {
      "id": 112,
      "name": "Hero (4th)"
    },
    {
      "id": 11212,
      "name": "Beast Tamer"
    },
    {
      "id": 120,
      "name": "Page (2nd)"
    },
    {
      "id": 1200,
      "name": "Blaze Wizard  (1st)"
    },
    {
      "id": 121,
      "name": "Knight (3rd)"
    },
    {
      "id": 1210,
      "name": "Blaze Wizard (2nd)"
    },
    {
      "id": 1211,
      "name": "Blaze Wizard (3rd)"
    },
    {
      "id": 1212,
      "name": "Blaze Wizard (4th)"
    },
    {
      "id": 122,
      "name": "Paladin (4th)"
    },
    {
      "id": 130,
      "name": "Spearman (2nd)"
    },
    {
      "id": 1300,
      "name": "Wind Archer (1st)"
    },
    {
      "id": 131,
      "name": "Dragon Knight (3rd)"
    },
    {
      "id": 1310,
      "name": "Wind Archer (2nd)"
    },
    {
      "id": 1311,
      "name": "Wind Archer (3rd)"
    },
    {
      "id": 1312,
      "name": "Wind Archer (4th)"
    },
    {
      "id": 132,
      "name": "Dark Knight (4th)"
    },
    {
      "id": 1400,
      "name": "Night Walker (1st)"
    },
    {
      "id": 1410,
      "name": "Night Walker (2nd)"
    },
    {
      "id": 1411,
      "name": "Night Walker (3rd)"
    },
    {
      "id": 1412,
      "name": "Night Walker (4th)"
    },
    {
      "id": 14200,
      "name": "Kinesis (1st)"
    },
    {
      "id": 14210,
      "name": "Kinesis (2nd)"
    },
    {
      "id": 14211,
      "name": "Kinesis (3rd)"
    },
    {
      "id": 14212,
      "name": "Kinesis (4th)"
    },
    {
      "id": 1500,
      "name": "Thunder Breaker (1st)"
    },
    {
      "id": 1510,
      "name": "Thunder Breaker (2nd)"
    },
    {
      "id": 1511,
      "name": "Thunder Breaker (3rd)"
    },
    {
      "id": 1512,
      "name": "Thunder Breaker (4th)"
    },
    {
      "id": 15200,
      "name": "Illium (1st)"
    },
    {
      "id": 15210,
      "name": "Illium (2nd)"
    },
    {
      "id": 15211,
      "name": "Illium (3rd)"
    },
    {
      "id": 15212,
      "name": "Illium (4th)"
    },
    {
      "id": 15500,
      "name": "Ark (1st)"
    },
    {
      "id": 15510,
      "name": "Ark (2nd)"
    },
    {
      "id": 15511,
      "name": "Ark (3rd)"
    },
    {
      "id": 15512,
      "name": "Ark (4th)"
    },
    {
      "id": 200,
      "name": "Magician (1st)"
    },
    {
      "id": 2000,
      "name": "Aran"
    },
    {
      "id": 2001,
      "name": "Evan (Beginner)"
    },
    {
      "id": 210,
      "name": "Wizard - Fire/Poison (2nd)"
    },
    {
      "id": 2100,
      "name": "Aran (1st)"
    },
    {
      "id": 211,
      "name": "Mage - Fire/Poison (3rd)"
    },
    {
      "id": 2110,
      "name": "Aran (2nd)"
    },
    {
      "id": 2111,
      "name": "Aran (3rd)"
    },
    {
      "id": 2112,
      "name": "Aran (4th)"
    },
    {
      "id": 212,
      "name": "Arch Mage F/P (4th)"
    },
    {
      "id": 220,
      "name": "Wizard - Ice/Lightning (2nd)"
    },
    {
      "id": 2200,
      "name": "Evan (1st)"
    },
    {
      "id": 221,
      "name": "Mage - Ice/Lightning (3rd)"
    },
    {
      "id": 2211,
      "name": "Evan (2nd)"
    },
    {
      "id": 2214,
      "name": "Evan (3rd)"
    },
    {
      "id": 2217,
      "name": "Evan (4th)"
    },
    {
      "id": 222,
      "name": "Arch Mage I/L (4th)"
    },
    {
      "id": 230,
      "name": "Cleric (2nd)"
    },
    {
      "id": 2300,
      "name": "Mercedes (1st)"
    },
    {
      "id": 231,
      "name": "Priest (3rd)"
    },
    {
      "id": 2310,
      "name": "Mercedes (2nd)"
    },
    {
      "id": 2311,
      "name": "Mercedes (3rd)"
    },
    {
      "id": 2312,
      "name": "Mercedes (4th)"
    },
    {
      "id": 232,
      "name": "Bishop (4th)"
    },
    {
      "id": 2400,
      "name": "Phantom (1st)"
    },
    {
      "id": 2410,
      "name": "Phantom (2nd)"
    },
    {
      "id": 2411,
      "name": "Phantom (3rd)"
    },
    {
      "id": 2412,
      "name": "Phantom (4th)"
    },
    {
      "id": 2510,
      "name": "Shade (2nd)"
    },
    {
      "id": 2511,
      "name": "Shade (3rd)"
    },
    {
      "id": 2512,
      "name": "Shade (4th)"
    },
    {
      "id": 2700,
      "name": "Luminous (1st)"
    },
    {
      "id": 2710,
      "name": "Luminous (2nd)"
    },
    {
      "id": 2711,
      "name": "Luminous (3rd)"
    },
    {
      "id": 2712,
      "name": "Luminous (4th)"
    },
    {
      "id": 300,
      "name": "Archer (1st)"
    },
    {
      "id": 3000,
      "name": "Citizen"
    },
    {
      "id": 310,
      "name": "Hunter (2nd)"
    },
    {
      "id": 3100,
      "name": "Demon Slayer (1st)"
    },
    {
        "id": 3101,
        "name": "Demon Avenger (1st)"
      },
      {
        "id": 3001,
        "name": "Demon Slayer (1st)"
      },
    {
      "id": 311,
      "name": "Ranger (3rd)"
    },
    {
      "id": 3110,
      "name": "Demon Slayer (2nd)"
    },
    {
      "id": 3111,
      "name": "Demon Slayer (3rd)"
    },
    {
      "id": 3112,
      "name": "Demon Slayer (4th)"
    },
    {
      "id": 312,
      "name": "Bow Master (4th)"
    },
    {
      "id": 3120,
      "name": "Demon Avenger (2nd)"
    },
    {
      "id": 3121,
      "name": "Demon Avenger (3rd)"
    },
    {
      "id": 3122,
      "name": "Demon Avenger (4th)"
    },
    {
      "id": 320,
      "name": "Crossbowman (2nd)"
    },
    {
      "id": 3200,
      "name": "Battle Mage (1st)"
    },
    {
      "id": 321,
      "name": "Sniper (3rd)"
    },
    {
      "id": 3210,
      "name": "Battle Mage (2nd)"
    },
    {
      "id": 3211,
      "name": "Battle Mage (3rd)"
    },
    {
      "id": 3212,
      "name": "Battle Mage (4th)"
    },
    {
      "id": 322,
      "name": "Marksman (4th)"
    },
    {
      "id": 3300,
      "name": "Wild Hunter (1st)"
    },
    {
      "id": 3310,
      "name": "Wild Hunter (2nd)"
    },
    {
      "id": 3311,
      "name": "Wild Hunter (3rd)"
    },
    {
      "id": 3312,
      "name": "Wild Hunter (4th)"
    },
    {
      "id": 3500,
      "name": "Mechanic (1st)"
    },
    {
      "id": 3510,
      "name": "Mechanic (2nd)"
    },
    {
      "id": 3511,
      "name": "Mechanic (3rd)"
    },
    {
      "id": 3512,
      "name": "Mechanic (4th)"
    },
    {
      "id": 3600,
      "name": "Xenon (1st)"
    },
    {
      "id": 3610,
      "name": "Xenon (2nd)"
    },
    {
      "id": 3611,
      "name": "Xenon (3rd)"
    },
    {
      "id": 3612,
      "name": "Xenon (4th)"
    },
    {
      "id": 3700,
      "name": "블래스터(1차)"
    },
    {
      "id": 3710,
      "name": "블래스터(2차)"
    },
    {
      "id": 3711,
      "name": "블래스터(3차)"
    },
    {
      "id": 3712,
      "name": "Blaster (4th)"
    },
    {
      "id": 400,
      "name": "Rogue (1st)"
    },
    {
      "id": 410,
      "name": "Assassin (1st)"
    },
    {
      "id": 4100,
      "name": "Hayato (1st)"
    },
    {
      "id": 411,
      "name": "Hermit (3rd)"
    },
    {
      "id": 4110,
      "name": "Hayato (2nd)"
    },
    {
      "id": 4111,
      "name": "Hayato (3rd)"
    },
    {
      "id": 4112,
      "name": "Hayato(4th)"
    },
    {
      "id": 412,
      "name": "Night Lord (4th)"
    },
    {
      "id": 420,
      "name": "Bandit (2nd)"
    },
    {
      "id": 4200,
      "name": "Kanna (1st)"
    },
    {
      "id": 421,
      "name": "Chief Bandit  (3rd)"
    },
    {
      "id": 4210,
      "name": "Kanna (2nd)"
    },
    {
      "id": 4211,
      "name": "Kanna (3rd)"
    },
    {
      "id": 4212,
      "name": "Kanna(4th)"
    },
    {
      "id": 422,
      "name": "Shadower (4th)"
    },
    {
      "id": 430,
      "name": "Blade Recruit (2nd)"
    },
    {
      "id": 431,
      "name": "Blade Acolyte  (3rd)"
    },
    {
      "id": 432,
      "name": "Blade Specialist (4th)"
    },
    {
      "id": 433,
      "name": "Blade Lord (5th)"
    },
    {
      "id": 434,
      "name": "Dual Blade (4th)"
    },
    {
      "id": 500,
      "name": "Pirate (1st)"
    },
    {
      "id": 501,
      "name": "Cannoneer (1st)"
    },
    {
      "id": 508,
      "name": "Jett (1st)"
    },
    {
      "id": 510,
      "name": "Brawler (2nd)"
    },
    {
      "id": 5100,
      "name": "Mihile (1st)"
    },
    {
      "id": 511,
      "name": "Marauder (3rd)"
    },
    {
      "id": 5110,
      "name": "Mihile (2nd)"
    },
    {
      "id": 5111,
      "name": "Mihile (3rd)"
    },
    {
      "id": 5112,
      "name": "Mihile (4th)"
    },
    {
      "id": 512,
      "name": "Buccaneer (4th)"
    },
    {
      "id": 520,
      "name": "Gunslinger (2nd)"
    },
    {
      "id": 521,
      "name": "Outlaw (3rd)"
    },
    {
      "id": 522,
      "name": "Corsair (4th)"
    },
    {
      "id": 530,
      "name": "Cannoneer (2nd)"
    },
    {
      "id": 531,
      "name": "Cannon Trooper (3rd)"
    },
    {
      "id": 532,
      "name": "Cannon Master (4th)"
    },
    {
      "id": 570,
      "name": "Jett (2nd)"
    },
    {
      "id": 571,
      "name": "Jett (3rd)"
    },
    {
      "id": 572,
      "name": "ZET(4th)"
    },
    {
      "id": 6100,
      "name": "Kaiser (1st)"
    },
    {
      "id": 6110,
      "name": "Kaiser (2nd)"
    },
    {
      "id": 6111,
      "name": "Kaiser (3rd)"
    },
    {
      "id": 6112,
      "name": "Kaiser (4th)"
    },
    {
      "id": 6400,
      "name": "Cadena (1st)"
    },
    {
      "id": 6410,
      "name": "Cadena (2nd)"
    },
    {
      "id": 6411,
      "name": "Cadena (3rd)"
    },
    {
      "id": 6412,
      "name": "Cadena (4th)"
    },
    {
      "id": 6500,
      "name": "Angelic Buster (1st)"
    },
    {
      "id": 6510,
      "name": "Angelic Buster (2nd)"
    },
    {
      "id": 6511,
      "name": "Angelic Buster (3rd)"
    },
    {
      "id": 6512,
      "name": "Angelic Buster (4th)"
    }
  ]