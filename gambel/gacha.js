const Discord = require('../node_modules/discord.js');
var sqlFunctions = require('../Brain/sqlFunctions');
var random = require('../Brain/random');

//let discordID = index.discordID;
//let msg = index.msg;



module.exports.doGacha = function(msg, discordID, args, celest) {

  if (msg.channel.id !== '608484416401965056') {
    msg.channel.send(`Please use the #Gachapon channel to use this command!`);
    return;
  }
  
  if (!msg.member.roles.has('602324496258695178')) {
    msg.channel.send('You must be a celestial member to gambel your mesos away! type "register" in my dms to make an account!');
    return;
  }

sqlFunctions.getResult('SELECT * FROM accounts WHERE discordID = ' + discordID + '', function (err, results) {
  if (results[0]) {
    var accountRow = JSON.parse(JSON.stringify(results[0]));

    let accountID = accountRow.id;

    let type = `item`;

    if (args[2]) {
      gachaType = args[2];
  
      switch (gachaType) {
        case `item`:
          type = `item`;
        break;
  
        case `chair`:
          type = `chair`;
        break;
  
        default:
          type = `item`;
        break;
      }
    }

    let vp = accountRow.vpoints;
    let gachaFeilds = new Array;

    let chairs = new Array;

    let tableName = `chairs`;
    

    sqlFunctions.getResult(`SELECT * FROM ${tableName}`, function(err, results) {
      for(var i = 0; i < results.length; i++){

        var rows = JSON.parse(JSON.stringify(results[i]));
            chairs.push(rows.itemid);
        }
        //console.log(chairs); -- this is disgusting lol
        let randomChair = chairs[Math.floor(Math.random() * chairs.length)];
        //console.log(randomChair);


    //console.log(`${msg.author.username} has ${vp} and is trying to get a chair`);

    //console.log(randomChair);

    let gachaEmbed;

    let finalItem;

    let getItemCommon = random.randomItemCommon[Math.floor(Math.random() * random.randomItemCommon.length)];
    let getItemRare = random.randomItemRare[Math.floor(Math.random() * random.randomItemRare.length)];
    let getItemUltra = random.randomItemUltra[Math.floor(Math.random() * random.randomItemUltra.length)];
    
    let teirUp = Math.floor(Math.random() * 1000);

    
    if (teirUp < 250) { //250
      finalItem = getItemRare;
      if (teirUp < 75) { // 75
        finalItem = getItemUltra;
        celest.channels.get('601996201386180619').send(`${msg.author} just got a ${finalItem.name} from the Celestial Gachapon! How lucky!`)
        msg.channel.send(`${msg.author} just got a ${finalItem.name} from the Celestial Gachapon! How lucky!`);
      }
    } else {
      finalItem = getItemCommon;
    }

    if (finalItem.rarity == 3) {
      let dmembed = new Discord.RichEmbed ({
        title: `Celest's Gachapon Logs`,
        fields: [
          {
            name: `${msg.author.username} Spun the Gachapon and rolled an Ultra Rare Item!`,
            value: `Item won: ${finalItem.name}`
          }
        ]
      });
      celest.channels.get("610570550472867853").send(dmembed)
    }

    //console.log(teirUp, finalItem);

    let finalitemID = finalItem.itemID;
    let itemType = finalitemID.substring(0, 3);


    

    

    sqlFunctions.getResult(`SELECT * FROM characters WHERE accountid = ${accountID}`, function(err, results) {

      if (results[0]) {
        let charRows = JSON.parse(JSON.stringify(results[0]));

        let eyes = charRows.face;
        let hair = charRows.hair;
        let id = charRows.id;

        let charItems = new Array;

        let addEquip = new Array;

        let weapon = new Array;
        let top = new Array;
        let bottom = new Array;
        let overall = new Array;
        let shoes = new Array;
        let earrings = new Array;
        let cape = new Array;
        let faceacc = new Array;
        let eyeacc = new Array;
        let hat = new Array;

        if (itemType) {

          console.log(itemType);
          switch (itemType) {
              case `100`: // Hat
                  hat.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `105`: //Overall
                  overall.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `103`: //Earring
                  earrings.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `104`: //Top
                  top.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `110`: // Cape
                  cape.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `101`: //FaceAcc
                  faceacc.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `102`: //Eye Acc
                  eyeacc.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `106`: //Pants
                  bottom.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `107`: //shoes
                  shoes.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `170`: //NX Weapon
                  weapon.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              case `130`: //1H Sword
                  weapon.push(finalitemID);
                  addEquip.push(`{"itemId":${finalitemID}}`);
              break;
              default: 
                weapon.push(finalitemID);
                addEquip.push(`{"itemId":${finalitemID}}`);
              break;
            }
          }
      

    sqlFunctions.getResult(`SELECT * FROM inventoryitems WHERE characterid = ${id} && inventorytype = -1`, function(err, results) {
        if (results[0]) {
            for(var i = 0; i < results.length; i++){
                var rows = JSON.parse(JSON.stringify(results[i]));
                charItems.push(rows.itemid);
                let currentEquip = `${rows.itemid}`;
                let thisEquip = currentEquip.substring(0, 3);


        

                
                if (thisEquip) {

                console.log(thisEquip);
                switch (thisEquip) {
                    case `100`: // Hat
                    if (hat.length == 0) {
                      hat.push(rows.itemid);
                      addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `105`: //Overall
                    if (overall.length == 0) {
                        overall.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `103`: //Earring
                    if (earrings.length == 0) {
                        earrings.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `104`: //Top
                    if (top.length == 0 && overall.length == 0) {
                        top.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `110`: // Cape
                    if (cape.length == 0) {
                        cape.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `101`: //FaceAcc
                    if (faceacc.length == 0) {
                        faceacc.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `106`: //Pants
                    if (bottom.length == 0 && overall.length == 0) {
                        bottom.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `107`: //shoes
                    if (shoes[0]) {
                        shoes.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `170`: //NX Weapon
                    if (weapon.length == 0) {
                        weapon.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                    case `130`: //1H Sword
                    if (weapon.length == 0) {
                        weapon.push(rows.itemid);
                        addEquip.push(`{"itemId":${rows.itemid}}`);
                    }
                    break;
                  }
                }

            }
        
    }


    //console.log(getItem.itemID);

    if (type == `item`) {
      const attachment = new Discord.Attachment(`./gambel/ChairGacha2/0${randomChair}.effect.0.png`, 'sample.png');
       gachaEmbed = new Discord.RichEmbed ({
        title: `🃏Celestial Gachapon🃏`,
            thumbnail: {
              url: `https://cdn.wikimg.net/en/strategywiki/images/1/1e/MS_NPC_The_Great_Gachapierrot.png`,
            },
            fields: gachaFeilds,
      });
      console.log(addEquip.toString());
     /* console.log(`Currently Equipped Items:
      \rHat: ${hat}
      \rOverall: ${overall}
      \rEarring: ${earrings}
      \rTop: ${top}
      \rPants: ${bottom}
      \rShoes: ${shoes}
      \rWeapon: ${weapon}
      \rCape: ${cape}`);*/
      gachaEmbed.image = {
        url: `https://maplestory.io/api/character/{"itemId":${hair}},{"itemId":${eyes},"animationName":"default"},${addEquip.toString()},{"itemId":2000,"region":"GMS","version":"latest"},{"itemId":12000,"region":"GMS","version":"latest"}/stand1/animated?showears=false&showLefEars=false&showHighLefEars=false&resize=1&name=&flipX=false`,
      }
    }

    if (type == `chair`) {
      const attachment = new Discord.Attachment(`./gambel/ChairGacha2/0${randomChair}.effect.0.png`, 'sample.png');
       gachaEmbed = new Discord.RichEmbed ({
        title: `🃏Celestial Chair Gachapon🃏`,
            thumbnail: {
              url: `https://cdn.wikimg.net/en/strategywiki/images/1/1e/MS_NPC_The_Great_Gachapierrot.png`,
            },
            fields: gachaFeilds,
      }).attachFile(attachment);
      gachaEmbed.image = {
        url: `attachment://sample.png`,
      }
    }

    //console.log(gachaEmbed.image.url);



    if (vp <= 0) {
      let noVp = Object.assign({}, gachaEmbed.fields[0]);
      noVp.name = `Oh no! You don't have any Vote Points!`;
      noVp.value = `Type "Celest vote" to get some now!`;
      gachaFeilds.push(noVp);
      msg.channel.send(gachaEmbed);
      return;
    }

    let gachaResultFeild = Object.assign({}, gachaEmbed.fields[0]);
    let remainingVp = Object.assign({}, gachaEmbed.fields[1]);



    let realVp = accountRow.vpoints - 1;

      sqlFunctions.getResult(`SELECT * FROM chairs WHERE itemid = '${randomChair}'`, function(err, results) {
        if (results[0]) {

      sqlFunctions.getResult(`UPDATE accounts SET vpoints = vpoints - 1 WHERE discordID = ${discordID}`, function(err, results) {});
      let chairRows = JSON.parse(JSON.stringify(results[0]));


      let itemName;
      let itemDesc;
      let reward;

      if (type == `chair`) {
         itemName = chairRows.name;
         itemDesc = chairRows.desc;
         reward = randomChair;
      }

      if (type == `item`) {
         itemName = finalItem.name;
         itemDesc = finalItem.desc;
         reward = finalItem.itemID;
      }


    gachaResultFeild.name = `Here's your results!\r\n`;
    gachaResultFeild.value = `Item: ${itemName}\r\nDescription: ${itemDesc}`;
      remainingVp.name = `You currently have ${realVp} vote points left!`;
      remainingVp.value = `- - - - - - - - - - - - - - - -`;


    gachaFeilds.push(remainingVp);
    gachaFeilds.push(gachaResultFeild);
    msg.channel.send(gachaEmbed);

    let generateCode = Math.floor(Math.random() * 1000000000);
      if (type !== `chair`) {
        return;
      }
    sqlFunctions.getResult(`INSERT INTO nxcode (code, valid, user, type, item, quantity) VALUES ('${generateCode}', '1', '${discordID}', '6', '${reward}', '1')`,function(err, results) {});
    gachaResultFeild.name = `Lucky you! You got a ${itemName}!`;
    gachaResultFeild.value = `\r\nNX Code : ${generateCode}\r\n Go Into The cash shop and redeem it!`;
    //gachaFeilds.push(gachaResultFeild);
    msg.author.send(gachaEmbed);
        } else {
          msg.channel.send(`Everything is being updated. Please stay tuned. uwu`);
        }
  });
  
});
} else {
  msg.channel.send(`You don't have any Characters in game!`); 
}
});
    return;
    });
    //msg.author.send(`Here are your marvel machine NX codes! uwu `);

    return;
  } else {
    msg.channel.send(`Check your DMs`);
    msg.author.send("hai it looks like you wanted to make an account, just say 'register' and we can go from there uwu");
    return;
  }
});
}
