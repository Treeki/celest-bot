const Discord = require('../node_modules/discord.js');
var sql = require('../Brain/sqlFunctions');



module.exports.doGambel = function(msg, discordID, randomThumb, args, prefix, celest) {

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  

  if (args[1] !== `flip`) {
    return;
  }

if (msg.channel.id !== '604752530760663051') {
  msg.channel.send(`Please use the #gamble channel to gamblel ur life away uwu`);
  return;
}

if (!msg.member.roles.has('602324496258695178')) {
  msg.channel.send('You must be a celestial member to gambel your mesos away! type "register" in my dms to make an account!');
  return;
}

/*if (discordID == `203732768125878272`) {
  msg.channel.send(`u have a problem stop`);
  return;
}*/

let type = `mesos`;

if (args[3]) {
  let flipType = args[3];

  switch (flipType) {
    case `vp`:
      type = `vpoints`;
    break;

    case `dp`:
      type = `dpoints`;
    break;

    case `mesos`:
      type = `mesos`;
    break;

    case `nx`:
      type = `nxCredit`;
    break;

    default:
      type = `mesos`;
    break;
  }
}



sql.getResult('SELECT * FROM bank WHERE discordID = ' + discordID + '', function (err, results) {
  if (results[0]) {
  var bankRows = JSON.parse(JSON.stringify(results[0]));
  
  sql.getResult('SELECT * FROM accounts WHERE discordID = ' + discordID + '', function (err, results) {
  if (results[0]) {
    var accRows = JSON.parse(JSON.stringify(results[0]));

    let flipFeilds = new Array;

    let winPercent = Math.round(( Number(bankRows.win)/(Number(bankRows.win) + Number(bankRows.loss))) * 100);

    const flipEmbed = new Discord.RichEmbed({
      title: `Let's see if your lucky today ${msg.author.username}`,
      thumbnail: {
            url: randomThumb,
          },
      //description: `Let's flip a coin!`,
          fields: [
          {
            name: `Win Percentage`,
            value: `${winPercent}%  `,
            inline: true,
          }],
    });

  

    if (type == `vpoints`) {

      flipEmbed.fields[1] = 
      {
        name: `Vote Point Profit`,
        value: `${numberWithCommas(bankRows.profitVp)}`,
        inline: true,
      }
    }

    if (type == `mesos`) {

      flipEmbed.fields[1] = 
      {
        name: `Meso Profit`,
        value: `${numberWithCommas(bankRows.profit)}`,
        inline: true,
      }
    }

    if (type == `dpoints`) {

      flipEmbed.fields[1] = 
      {
        name: `Crystal Profit`,
        value: `${numberWithCommas(bankRows.profitDp)}`,
        inline: true,
      }
    }

    
    if (type == `nxCredit`) {

      flipEmbed.fields[1] = 
      {
        name: `Cash Profit`,
        value: `${numberWithCommas(bankRows.profitNx)}`,
        inline: true,
      }
    }



    var flipPush = Object.assign({}, flipEmbed.fields[3]);
    let playerGuess = args[1];
    let playerBet = args[2];
    var didWin = false;
    //flipPush.inline = true;

    if (!playerGuess || !playerBet) {
        flipEmbed.fields[0] = 
        {
          name: `Win Percentage`,
          value: `${winPercent}%  ${Number(bankRows.win)}/${Number(bankRows.loss)}`,
          inline: true,
        }
        flipEmbed.fields[1] = 
        {
          name: `Crystal Profit`,
          value: `${numberWithCommas(bankRows.profitDp)}`,
          inline: true,
        }
        flipEmbed.fields[2] = 
        {
          name: `Vote Profit`,
          value: `${numberWithCommas(bankRows.profitVp)}`,
          inline: true,
        }
        flipEmbed.fields[3] = 
        {
          name: `Meso Profit`,
          value: `${numberWithCommas(bankRows.profit)}`,
          inline: true,
        }
        flipEmbed.fields[4] = 
        {
          name: `Nx Profit`,
          value: `${numberWithCommas(bankRows.profitNx)}`,
          inline: true,
        }
        flipEmbed.footer = {
          text: 'Want to reset your stats? Say "Celest flip reset"',
          //icon_url: 'https://i.imgur.com/wSTFkRM.png',
        }
      msg.channel.send(flipEmbed);
      return;
    }

    if (msg.content.includes(`-`)) {
      flipPush.name = `Hm something looks wrong to me`;
      flipPush.value = `Why are you trying to break me?`;
      flipEmbed.fields.push(flipPush);
      msg.channel.send(flipEmbed);
      return;
    }


    //console.log(`${msg.author.username} played a bet ${playerBet.length} characters long`);
    if (playerBet.length > 8) {
      flipPush.name = `Hm something looks wrong to me`;
      flipPush.value = `You can't bet more than 99,999,999!`;
      flipEmbed.fields.push(flipPush);
      msg.channel.send(flipEmbed);
      return;
    }



    if (isNaN(playerBet) && playerBet !== `all` && playerBet !== `half` && playerBet !== `reset`) {
      //console.log('is not a number!');
      flipPush.name = `Hm something looks wrong to me`;
      flipPush.value = `are you sure ${playerBet} is a number?`;
      flipEmbed.fields.push(flipPush);
      msg.channel.send(flipEmbed);
      return;
    }

    if (playerBet === `all`) {
      switch(type) {
        case `nxCredit`:
        playerBet = accRows.nxCredit;
        break;
        case `vpoints`:
        playerBet = accRows.vpoints;
        break;
        case `dpoints`:
        playerBet = accRows.dpoints;
        break;
        default:
        playerBet = bankRows.mesos;
        break;
      }
    }

    if (playerBet === `half`) {
      
      switch(type) {
        case `nxCredit`:
        playerBet = Math.round(accRows.nxCredit* 0.5);
        break;
        case `vpoints`:
        playerBet = Math.round(accRows.vpoints* 0.5);
        break;
        case `dpoints`:
        playerBet = Math.round(accRows.dpoints* 0.5);
        break;
        default:
          playerBet = Math.round(bankRows.mesos* 0.5);
        break;
      }
    }

    if (playerBet === `reset`) {
      sql.getResult(`UPDATE bank SET profit=0, win=0, loss=0, profitVp=0, profitDp=0 WHERE discordID = '${discordID}'`, function(err, results) {});
      flipEmbed.fields[0] = 
      {
        name: `Your stats have been reset!`,
        value: `Everything has been set to 0.`,
      }
      flipEmbed.fields[1] = 
      {
        name: `Don't worry though! All of your earned mesos, vp, dp are still there!`,
        value: `You can start your stats again right now by playing!`,
      }
      msg.channel.send(flipEmbed);
      return;
    }


    var flip = `lose`;

    var flipResult = Math.floor(Math.random() * 100);
    console.log(`Flip Result: ${flipResult}`);


    if (flipResult <= 40) {
      var flip = `win`;
      //winPercent = Math.round(((bankRows.win + 1 )/ bankRows.loss) * 100);
    }

    

    if (type == `vpoints` || type == `dpoints` || type == `nxCredit`) {

      

      let checkVp = accRows.vpoints;
      let checkDp = accRows.dpoints;
      let checkNx = accRows.nxCredit;

  
      if (type == `vpoints`) {
      if (Number(playerBet) > Number(checkVp)) {
        let notEnough;
        let checkNum;
        if (type == `vpoints`) { notEnough = `Are you sure you have that many Vote Points?`; checkNum = checkVp;}
        if (type == `dpoints`) { notEnough = `Are you sure you have that many Crystals?`; checkNum = checkDp; }
        if (type == `nxCredit`) { notEnough = `Are you sure you that much Celestial Cash?`; checkNum = checkNx; }
        flipPush.name = `${notEnough}`;
        flipPush.value = `it seems you only have ${numberWithCommas(checkNum)} ${type} left!`;
        flipEmbed.fields.push(flipPush);
        msg.channel.send(flipEmbed);
        return;
        }
      }
      if (type == `dpoints`) {
        if (Number(playerBet) > Number(checkDp)) {
          let notEnough;
          let checkNum;
          if (type == `vpoints`) { notEnough = `Are you sure you have that many Vote Points?`; checkNum = checkVp;}
          if (type == `dpoints`) { notEnough = `Are you sure you have that many Crystals?`; checkNum = checkDp; }
          if (type == `nxCredit`) { notEnough = `Are you sure you that much Celestial Cash?`; checkNum = checkNx; }
          flipPush.name = `${notEnough}`;
          flipPush.value = `it seems you only have ${numberWithCommas(checkNum)} ${type} left!`;
          flipEmbed.fields.push(flipPush);
          msg.channel.send(flipEmbed);
          return;
          }
        }
          if (type == `nxCredit`) {
          if (Number(playerBet) > Number(checkNx)) {
            let notEnough;
            let checkNum;
            if (type == `vpoints`) { notEnough = `Are you sure you have that many Vote Points?`; checkNum = checkVp;}
            if (type == `dpoints`) { notEnough = `Are you sure you have that many Crystals?`; checkNum = checkDp; }
            if (type == `nxCredit`) { notEnough = `Are you sure you that much Celestial Cash?`; checkNum = checkNx; }
            flipPush.name = `${notEnough}`;
            flipPush.value = `it seems you only have ${numberWithCommas(checkNum)} ${type} left!`;
            flipEmbed.fields.push(flipPush);
            msg.channel.send(flipEmbed);
            return;
            }
          }
      
  

  
     
    
      

      if (playerBet > 500 && type == `dpoints`) {
        notEnough = `Max Crystal Bet is 5$!`; checkNum = checkDp;
        flipPush.name = `${notEnough}`;
        flipPush.value = `Please don't bet ${numberWithCommas(playerBet)}`;
        flipEmbed.fields.push(flipPush);
      msg.channel.send(flipEmbed);
      return;
      }


      
      if (playerBet > 5 && type == `vpoints`) {
        notEnough = `Max Vote Point bet is 5!`; checkNum = checkVp;
        flipPush.name = `${notEnough}`;
        flipPush.value = `Please don't bet ${numberWithCommas(playerBet)}`;
        flipEmbed.fields.push(flipPush);
      msg.channel.send(flipEmbed);
      return;
      }

      if (playerBet > 99999 && type == `nxCredit`) {
        notEnough = `Max NX Bet is 99,999`; checkNum = checkDp;
        flipPush.name = `${notEnough}`;
        flipPush.value = `Please don't bet ${numberWithCommas(playerBet)}`;
        flipEmbed.fields.push(flipPush);
      msg.channel.send(flipEmbed);
      return;
      }

    
        

      
    
    

      if (flip == `win`) {
        let winAmount = playerBet;
        let winBalance;
        let winResponse;

        //let winBalance = Number(winAmount) + Number(mesos.mesos);

        if (type == `vpoints`) {
          sql.getResult(`UPDATE accounts SET vpoints=vpoints+${winAmount} WHERE discordID = ${discordID}`,function(err, results) {});
          sql.getResult(`UPDATE bank SET profitVp=profitVp+${playerBet}, win=win+1 WHERE discordID = ${discordID}`,function(err, results) {});
          winBalance = Number(winAmount) + Number(accRows.vpoints);
          winResponse = `You gained ${numberWithCommas(winAmount)} Vote Points!`;
        }
        if (type == `dpoints`) {
          sql.getResult(`UPDATE accounts SET dpoints=dpoints+${winAmount} WHERE discordID = ${discordID}`,function(err, results) {});
          sql.getResult(`UPDATE bank SET profitDp=profitDp+${playerBet}, win=win+1  WHERE discordID = ${discordID}`,function(err, results) {});
          winBalance = Number(winAmount) + Number(accRows.dpoints);
          winResponse = `You gained ${numberWithCommas(winAmount)} Donation Crystals! <a:Crystal:606974054947356699>`;
        }
        if (type == `nxCredit`) {
          sql.getResult(`UPDATE accounts SET nxCredit=nxCredit+${winAmount} WHERE discordID = ${discordID}`,function(err, results) {});
          sql.getResult(`UPDATE bank SET profitNx=profitNx+${playerBet}, win=win+1  WHERE discordID = ${discordID}`,function(err, results) {});
          winBalance = Number(winAmount) + Number(accRows.dpoints);
          winResponse = `You gained ${numberWithCommas(winAmount)} Celestial Cash!`;
        }
        
        flipEmbed.description = `Your new Balanace is ${numberWithCommas(winBalance)} ${type}`;
        flipPush.name = `It Looks like you won!`;
        flipPush.value = `${winResponse}`;
        flipEmbed.fields.push(flipPush);
        if (winAmount >= 1000) {
          let dmembed = new Discord.RichEmbed ({ 
            title: `Gamble Log`,
            fields: [
                {
                    name: `${msg.author.username} Just won a flip!`,
                    value: `They Gained ${numberWithCommas(winAmount)}\r${type}`
                },
          ]
        });
        celest.channels.get("610570550472867853").send(dmembed)
        }

        msg.channel.send(flipEmbed);
        return;
      } else {
        let lossBalance;
        let gainMessage;
        if (type == `vpoints`) {
          sql.getResult(`UPDATE accounts SET vpoints=vpoints-${playerBet} WHERE discordID = ${discordID}`,function(err, results) {});
          sql.getResult(`UPDATE bank SET profitVp=profitVp-${playerBet}, win=win-1  WHERE discordID = ${discordID}`,function(err, results) {});
           lossBalance = accRows.vpoints - playerBet;
           gainMessage = `Vote points!`;
        }
        if (type == `dpoints`) {
          sql.getResult(`UPDATE accounts SET dpoints=dpoints-${playerBet} WHERE discordID = ${discordID}`,function(err, results) {});
          sql.getResult(`UPDATE bank SET profitDp=profitDp-${playerBet}, win=win-1  WHERE discordID = ${discordID}`,function(err, results) {});
           lossBalance = accRows.dpoints - playerBet;
           gainMessage = `Donation Crystals!`;
        }
        if (type == `nxCredit`) {
          sql.getResult(`UPDATE accounts SET nxCredit=nxCredit-${playerBet} WHERE discordID = ${discordID}`,function(err, results) {});
          sql.getResult(`UPDATE bank SET profitNx=profitNx-${playerBet}, win=win-1  WHERE discordID = ${discordID}`,function(err, results) {});
           lossBalance = accRows.dpoints - playerBet;
           gainMessage = `Celestial Cash!`;
        }
        flipEmbed.description = `Your new Balanace is ${numberWithCommas(lossBalance)} ${gainMessage} <:meso:605108944184147970>`;
        flipPush.name = `Oh no you lost!`;
        flipPush.value = `You lost ${numberWithCommas(playerBet)} ${gainMessage} <a:tapYayy:602748143213477888>`;
        flipEmbed.fields.push(flipPush);

        msg.channel.send(flipEmbed);
        return;
      }

    } else {


        sql.getResult(`SELECT mesos FROM bank WHERE discordID = ${discordID}`, function (err, results) {
          var mesos = JSON.parse(JSON.stringify(results[0]));

          if (playerBet > mesos.mesos) {
            flipPush.name = `Are you sure you have that many mesos?`;
            flipPush.value = `it seems you only have ${numberWithCommas(mesos.mesos)} mesos left!`;
            flipEmbed.fields.push(flipPush);
            msg.channel.send(flipEmbed);
            return;
          }

          if (!playerBet) {
            flipPush.name = `Tell me how many mesos you want to bet!`;
            flipPush.value = `You currently have ${numberWithCommas(mesos.mesos)} Mesos`;
            flipEmbed.fields.push(flipPush);
            msg.channel.send(flipEmbed);
            return;
          }
          console.log(`${msg.author.username} just bet ${playerBet} with the flip command and ${flip}`);

          if (flip == `win`) {
            let winAmount = playerBet;

            let winBalance = Number(winAmount) + Number(mesos.mesos);


            sql.getResult(`UPDATE bank SET mesos=mesos+${winAmount}, profit=profit+${winAmount}, win=win+1 WHERE discordID = ${discordID}`,function(err, results) {

            });
            flipEmbed.description = `Your new Balanace is ${numberWithCommas(winBalance)} Mesos! <:meso:605108944184147970>`;
            flipPush.name = `It Looks like you won!`;
            flipPush.value = `You gained ${numberWithCommas(winAmount)} mesos! <:meso:605108944184147970>`;
            flipEmbed.fields.push(flipPush);
            msg.channel.send(flipEmbed);
            return;
          } else {
            let lossBalance = mesos.mesos - playerBet;
            sql.getResult(`UPDATE bank SET mesos=mesos-${playerBet}, profit=profit-${playerBet}, loss=loss+1 WHERE discordID = ${discordID}`,function(err, results) {

            });
            flipEmbed.description = `Your new Balanace is ${numberWithCommas(lossBalance)} Mesos! <:meso:605108944184147970>`;
            flipPush.name = `Oh no you lost!`;
            flipPush.value = `You lost ${numberWithCommas(playerBet)} mesos! <a:tapYayy:602748143213477888>`;
            flipEmbed.fields.push(flipPush);
            msg.channel.send(flipEmbed);
            return;
          }

        });

      }
    


  } else {
    msg.channel.send(`Type "Celest Help"`);
    msg.author.send("hai it looks like you wanted to make an account, just say 'register' and we can go from there uwu");
    return;
  }
});
} else {
  msg.channel.send(`Type "Celest Help"`);
}
});
}
